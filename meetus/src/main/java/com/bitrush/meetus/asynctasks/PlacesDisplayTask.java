package com.bitrush.meetus.asynctasks;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import com.bitrush.meetus.R;
import com.bitrush.meetus.fragments.AddMeetUpMapFragment;;
import com.bitrush.meetus.model.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.*;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by vivek on 4/4/15.
 */
public class PlacesDisplayTask extends AsyncTask<Object, Integer, List<HashMap<String, String>>> {

    private final AddMeetUpMapFragment addMeetUpMapFragment;
    JSONObject googlePlacesJson;
    GoogleMap googleMap;

    public PlacesDisplayTask(AddMeetUpMapFragment addMeetUpMapFragment) {
        this.addMeetUpMapFragment = addMeetUpMapFragment;
    }

    @Override
    protected List<HashMap<String, String>> doInBackground(Object... inputObj) {

        List<HashMap<String, String>> googlePlacesList = null;
        Places placeJsonParser = new Places();

        try {
            googleMap = (GoogleMap) inputObj[0];
            googlePlacesJson = new JSONObject((String) inputObj[1]);
            googlePlacesList = placeJsonParser.parse(googlePlacesJson);
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
        return googlePlacesList;
    }

    @Override
    protected void onPostExecute(List<HashMap<String, String>> list) {
        googleMap.clear();
        AddMeetUpMapFragment.addMarkerToMap(null);
        HashMap<Marker, com.bitrush.meetus.model.Location> markers = new HashMap<>();
        BitmapDescriptor bitmapDescriptor= BitmapDescriptorFactory.fromResource(R.drawable.ic_poi);
        for (HashMap<String, String> aList : list) {
            MarkerOptions markerOptions = new MarkerOptions();
            double lat = Double.parseDouble(aList.get("lat"));
            double lng = Double.parseDouble(aList.get("lng"));
            String placeName = aList.get("place_name");
            String vicinity = aList.get("vicinity");
            LatLng latLng = new LatLng(lat, lng);

            markerOptions.position(latLng);
            markerOptions.icon(bitmapDescriptor);
            markerOptions.title(placeName + "," + vicinity);
            Marker m = googleMap.addMarker(markerOptions);

            com.bitrush.meetus.model.Location location = new com.bitrush.meetus.model.Location();
            location.setDestinationLatitude(aList.get("lat"));
            location.setDestinationLongitude(aList.get("lng"));
            location.setLocationAddress(vicinity);
            location.setLocationDescription(placeName);
            markers.put(m, location);

            addMeetUpMapFragment.setMarkers(markers);
        }
    }
}
