package com.bitrush.meetus.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.bitrush.meetus.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 10/19/14.
 */
public class LeftNavDrawerAdapter  extends ArrayAdapter {

    private List<String> listNavItems;
    private Context mContext;
    class Holder {
        private TextView textView;
        public Holder(View view) {
            textView = (TextView) view.findViewById(R.id.titleLeftNav);
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }
    }

    public LeftNavDrawerAdapter(Context context, int resource) {
        super(context, resource);
        mContext = context;
        initList();
    }

    private void initList()
    {
        listNavItems = new ArrayList<String>();
        listNavItems.add("Home");
        listNavItems.add("Contacts");
        listNavItems.add("My MeetUps");
    }

    @Override
    public int getCount() {
        return listNavItems.size();
    }

    @Override
    public int getPosition(Object item) {
        return listNavItems.indexOf(item);
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Holder holder;
        View row = convertView;
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.left_nav_item, null);
            holder = new Holder(row);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        holder.getTextView().setText(listNavItems.get(position));
        return row;
    }
}
