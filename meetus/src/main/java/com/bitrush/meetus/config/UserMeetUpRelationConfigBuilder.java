package com.bitrush.meetus.config;

public class UserMeetUpRelationConfigBuilder {
    private boolean isUserAdmin;
    private boolean isGoing;

    public UserMeetUpRelationConfigBuilder setIsUserAdmin(boolean isUserAdmin) {
        this.isUserAdmin = isUserAdmin;
        return this;
    }

    public UserMeetUpRelationConfigBuilder setIsGoing(boolean isGoing) {
        this.isGoing = isGoing;
        return this;
    }

    public UserMeetUpRelationConfig createMeetupDetailsFragmentConfig() {
        return new UserMeetUpRelationConfig(isUserAdmin, isGoing);
    }
}