package com.bitrush.meetus.config;

public class WebSocketConfigBuilder {
    private String meetupId;
    private Integer userContryCode;
    private Long userPhoneNumber;
    private String token;
    private String userName;

    public WebSocketConfigBuilder withMeetupId(String meetupId) {
        this.meetupId = meetupId;
        return this;
    }

    public WebSocketConfigBuilder withCountryCode(Integer countryCode) {
        this.userContryCode = countryCode;
        return this;
    }

    public WebSocketConfigBuilder withUserPhoneNumber(Long phoneNumber) {
        this.userPhoneNumber = phoneNumber;
        return this;
    }

    public WebSocketConfigBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public WebSocketConfigBuilder withUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public WebSocketConfig build() {
        return new WebSocketConfig(meetupId,userContryCode,userPhoneNumber,token, userName);
    }

}
