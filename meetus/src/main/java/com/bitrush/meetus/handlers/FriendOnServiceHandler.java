package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.SharedPreferencesUtil;
import com.google.gson.Gson;

import java.util.List;

/**
 * @author ge3k on 20/9/14.
 */
public class FriendOnServiceHandler extends Handler {
    private LoginRegisterActivity parentActivity;

    public FriendOnServiceHandler(LoginRegisterActivity parentActivity) {
        this.parentActivity = parentActivity;
    }

    @Override
    public void handleMessage(Message msg) {
        String friendsOnService = (String) msg.obj;
        Gson gson = new Gson();
        String serviceResponse = gson.toJson(friendsOnService);
        SharedPreferencesUtil.write(parentActivity, MeetusConstants.KEY_FRIEND_DETAIL_LIST,serviceResponse);
        Toast.makeText(parentActivity.getApplicationContext(), "Sync Complete", Toast.LENGTH_LONG).show();
    }

    public FragmentActivity getParentActivity() {
        return parentActivity;
    }

    public void setParentActivity(LoginRegisterActivity parentActivity) {
        this.parentActivity = parentActivity;
    }
}
