package com.bitrush.meetus.model;

/**
 * Created by vivek on 4/26/15.
 */
public class CountryCode {
    private int code;
    private String name;

    public CountryCode(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public CountryCode() {}

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static CountryCode getCountryCodeFromString(String input) {
        String[] parts = input.split("\\(\\+");
        CountryCode newCountryCode = new CountryCode();
        newCountryCode.setName(parts[0].trim());
        newCountryCode.setCode(Integer.parseInt(parts[1].substring(0, parts[1].length() - 1)));
        return newCountryCode;
    }

    @Override
    public String toString() {
        return getName() + " (+" + getCode() + ")";
    }
}
