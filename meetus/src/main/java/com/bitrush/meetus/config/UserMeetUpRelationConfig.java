package com.bitrush.meetus.config;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vivek on 2/1/15.
 */
public class UserMeetUpRelationConfig implements Parcelable {
    public boolean isUserAdmin;
    public boolean isGoing;

    public UserMeetUpRelationConfig(boolean isUserAdmin, boolean isGoing) {
        this.isUserAdmin = isUserAdmin;
        this.isGoing = isGoing;
    }

    public static final Parcelable.Creator<UserMeetUpRelationConfig> CREATOR = new Parcelable.Creator<UserMeetUpRelationConfig>() {

        @Override
        public UserMeetUpRelationConfig createFromParcel(Parcel source) {
            return new UserMeetUpRelationConfig(source);
        }

        @Override
        public UserMeetUpRelationConfig[] newArray(int size) {
            return new UserMeetUpRelationConfig[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBooleanArray(new boolean[]{this.isUserAdmin, this.isGoing});
    }

    private UserMeetUpRelationConfig(Parcel in) {
        boolean[] booleans = new boolean[2];
        in.readBooleanArray(booleans);
        this.isGoing = booleans[1];
        this.isUserAdmin = booleans[0];
    }
}
