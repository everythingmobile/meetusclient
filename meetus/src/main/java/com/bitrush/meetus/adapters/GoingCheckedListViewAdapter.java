package com.bitrush.meetus.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bitrush.meetus.R;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.fragments.InviteParticipantsFragment;
import com.bitrush.meetus.model.ContactInfo;
import com.bitrush.meetus.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 1/19/15.
 */
public class GoingCheckedListViewAdapter extends ArrayAdapter<ContactInfo> {
    private final UserMeetUpRelationConfig config;
    private final ListView thisListView;
    private List<UserInfo> participants;
    private List<ContactInfo> participantsContactInfo;
    private Context context;
    private InviteParticipantsFragment parentFragment;

    static class ViewHolder {
        RadioButton isCheckedRB;
        TextView contactViewTV;
    }

    public GoingCheckedListViewAdapter(Context context, int resource, List<UserInfo> participants, ListView thisListView,
                                       InviteParticipantsFragment parentFragment, UserMeetUpRelationConfig config) {
        super(context, resource);
        this.participants = participants;
        this.context = context;
        this.config = config;
        this.thisListView = thisListView;
        this.parentFragment = parentFragment;
        setUpContactInfoList(participants);
    }

    private void setUpContactInfoList(List<UserInfo> participantsList) {
        participantsContactInfo = new ArrayList<>();
        for(UserInfo userInfo : participantsList) {
            participantsContactInfo.add(new ContactInfo(userInfo, true));
        }
    }

    @Override
    public int getCount() {
        return participantsContactInfo.size();
    }

    @Override
    public ContactInfo getItem(int position) {
        return participantsContactInfo.get(position);
    }

    @Override
    public int getPosition(ContactInfo item) {
        return participants.indexOf(item.getUserInfo());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.invite_list_item, null, false);

            viewHolder = new ViewHolder();
            viewHolder.contactViewTV = (TextView) convertView.findViewById(R.id.contactName);
            viewHolder.isCheckedRB = (RadioButton) convertView.findViewById(R.id.isCheckedRB);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.contactViewTV.setText(getItem(position).getUserInfo().getUserName());
        viewHolder.isCheckedRB.setChecked(getItem(position).isInvited());
        if (config.isGoing) {
            setListener(convertView, viewHolder.isCheckedRB, position);
        }
        return convertView;
    }

    private void setListener(View convertView, final RadioButton radioButton, final int position) {
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton.isChecked()) {
                    participantsContactInfo.get(position).setInvited(false);
                    participants.remove(participantsContactInfo.get(position).getUserInfo());
                    parentFragment.removedUser(participantsContactInfo.get(position).getUserInfo());
                } else {
                    participantsContactInfo.get(position).setInvited(true);
                    participants.add(participantsContactInfo.get(position).getUserInfo());
                    parentFragment.addedUser(participantsContactInfo.get(position).getUserInfo());
                }
                radioButton.setChecked(participantsContactInfo.get(position).isInvited());
                parentFragment.participantsChanged(false);
            }
        });
    }

    public void participantsChanged() {
        setUpContactInfoList(participants);
        notifyDataSetInvalidated();
        setListViewHeightBasedOnChildren(thisListView);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
