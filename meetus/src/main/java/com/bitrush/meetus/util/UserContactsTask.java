package com.bitrush.meetus.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserInfo;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ge3k on 20/9/14.
 */
public class UserContactsTask extends AsyncTask<Void,Void,List<UserInfo>> {

    private FragmentActivity parentActivity;
    private Handler userContactsHandler;
    private List<UserInfo> userDetailList;

    public UserContactsTask(FragmentActivity parentActivity, Handler messageHandler) {
        this.parentActivity = parentActivity;
        this.userContactsHandler = messageHandler;
    }


    public FragmentActivity getParentActivity() {
        return parentActivity;
    }

    public void setParentActivity(FragmentActivity parentActivity) {
        this.parentActivity = parentActivity;
    }

    @Override
    protected List<UserInfo> doInBackground(Void... params) {
        List<UserInfo> userInfoList = new ArrayList<UserInfo>();

        ContentResolver cr = parentActivity.getContentResolver();
        Uri simUri = ContactsContract.Contacts.CONTENT_URI;
        Cursor cur = cr.query(simUri,
                null, null, null, null);
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                UserInfo userInfo = new UserInfo();
                userInfo.setUserName(name);
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                            new String[]{id}, null);

                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String emailAddress = pCur.
                                getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                        if (phoneNo != null) {
                            phoneNo = PhoneNumberUtils.stripSeparators(phoneNo);
                            Phonenumber.PhoneNumber phoneNumber = null;
                            try {
                                phoneNumber = phoneNumberUtil.parse(phoneNo, StaticFunctions.getRegionOfUser(parentActivity));
                            } catch (NumberParseException e) {
                                e.printStackTrace();
                            }
                            if (phoneNumber != null) {
                                userInfo.setPhoneInfo(new PhoneInfo(phoneNumber.getCountryCode(), phoneNumber.getNationalNumber()));
                            } else {
                                Log.e("ERROR!","Phone number is null!!");
                            }
                            userInfoList.add(userInfo);

                        }
                    }
                    pCur.close();
                }
            }
        }
       /* Cursor cursorSim = null;
        try
        {
            String ClsSimPhonename = null;
            String ClsSimphoneNo = null;
            Pattern numberPattern = Pattern.compile("\\d");

            Uri simUri = Uri.parse("content://icc/adn");
            cursorSim = parentActivity.getContentResolver().query(simUri,null,null,null,null);
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            Log.i("PhoneContact", "total: "+cursorSim.getCount());

            while (cursorSim.moveToNext())
            {
                UserInfo userInfo = new UserInfo();
                ClsSimPhonename =cursorSim.getString(cursorSim.getColumnIndex("name"));
                ClsSimphoneNo = cursorSim.getString(cursorSim.getColumnIndex("number"));
                ClsSimphoneNo.replaceAll("\\D","");
                ClsSimphoneNo.replaceAll("&", "");
                ClsSimPhonename=ClsSimPhonename.replace("|","");
                if (ClsSimphoneNo != null) {
                    ClsSimphoneNo = PhoneNumberUtils.stripSeparators(ClsSimphoneNo);
                    if (ClsSimphoneNo.charAt(0) == '+') {
                        ClsSimphoneNo = ClsSimphoneNo.substring(1,ClsSimphoneNo.length());
                    }

                    if (!isNumeric(ClsSimphoneNo)) {
                        continue;
                    }
                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(ClsSimphoneNo, StaticFuntions.getRegionOfUser(parentActivity));
                    userInfo.setPhoneInfo(new PhoneInfo(phoneNumber.getCountryCode(), phoneNumber.getNationalNumber()));
                    userInfo.setUserName(ClsSimPhonename);
                    userInfoList.add(userInfo);
                }
                Log.i(MeetusConstants.APPTAG,"PhoneContact name: "+ClsSimPhonename+" phone: "+ClsSimphoneNo);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (cursorSim != null) {
                cursorSim.close();
            }
        }*/
        this.userDetailList = userInfoList;
        Log.i(MeetusConstants.APPTAG,userInfoList.toString());
        return userInfoList;
    }

    public boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(List<UserInfo> userInfos) {
        super.onPostExecute(userInfos);
        Log.i(MeetusConstants.APPTAG,"The list is" + userInfos);
        Message handlerMessage = new Message();
        handlerMessage.obj = userDetailList;
        userContactsHandler.sendMessage(handlerMessage);
    }

    public Handler getUserContactsHandler() {
        return userContactsHandler;
    }

    public void setUserContactsHandler(Handler userContactsHandler) {
        this.userContactsHandler = userContactsHandler;
    }
}
