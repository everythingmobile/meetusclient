package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.R;

public class AppStartYesNoFragment extends Fragment {

    private static LoginRegisterActivity parentActivity;
    private static View currentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        currentView = inflater.inflate(R.layout.fragment_app_start_yes_no, container, false);
        attachListenersToButtons();
        return currentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public static AppStartYesNoFragment newInstance(LoginRegisterActivity loginRegisterActivity) {
        AppStartYesNoFragment appStartYesNoFragment = new AppStartYesNoFragment();
        AppStartYesNoFragment.parentActivity = loginRegisterActivity;
        return appStartYesNoFragment;
    }

    private void attachListenersToButtons() {
        Button yesButton = (Button) currentView.findViewById(R.id.yesButton);
        Button noButton = (Button) currentView.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.yesNoButtonPressed(true);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.yesNoButtonPressed(false);
            }
        });
    }
}
