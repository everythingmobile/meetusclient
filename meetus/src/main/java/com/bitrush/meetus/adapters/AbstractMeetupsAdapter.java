package com.bitrush.meetus.adapters;

import android.support.v7.widget.RecyclerView;

import com.bitrush.meetus.model.MeetUpEntity;

import java.util.List;

/**
 * Created by ge3k on 16/05/15.
 */
public abstract class AbstractMeetupsAdapter extends RecyclerView.Adapter {
    public abstract void setMeetUps(List<MeetUpEntity> meetUps);
}
