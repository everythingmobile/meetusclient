package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.CommentsFragment;
import com.bitrush.meetus.model.UserAndUserComment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 4/18/15.
 */
public class GetCommentsHandler extends Handler {
    private CommentsFragment commentsFragment;

    public GetCommentsHandler(CommentsFragment commentsFragment) {
        this.commentsFragment = commentsFragment;
    }

    @Override
    public void handleMessage(Message msg) {
        String response = (String)msg.obj;
        if (response == null || response.equals("")) {
            return;
        }
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<UserAndUserComment>>(){}.getType();
        List<UserAndUserComment> userAndUserComments = gson.fromJson(response, listType);
        commentsFragment.setComments(userAndUserComments);
    }
}
