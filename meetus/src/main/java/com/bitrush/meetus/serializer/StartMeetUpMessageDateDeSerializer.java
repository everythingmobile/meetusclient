package com.bitrush.meetus.serializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vivek on 1/18/15.
 */
public class StartMeetUpMessageDateDeSerializer implements JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy h:mm:ss a");
        Date date = null;
        try {
            date = sdf.parse(json.getAsJsonPrimitive().getAsString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
