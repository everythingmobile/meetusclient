package com.bitrush.meetus.model;

import java.io.Serializable;

/**
 * @author ge3k on 5/7/14.
 */
public class MeetusUserDetail implements Serializable {
    private String longitude;
    private String latitude;
    private String id;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "MeetusUserDetail{" +
                "longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
