package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.RegisterFragment;

/**
 * Created by vivek on 9/14/14.
 */
public class UserAddTaskHandler extends Handler {
    private RegisterFragment registerFragment;
    @Override
    public void handleMessage(Message msg) {
        String message = (String)msg.obj;
        if(message!=null && message.equals("Success")) {
            registerFragment.userAdded();
        }
    }

    public void setRegisterFragment(RegisterFragment registerFragment) {
        this.registerFragment = registerFragment;
    }
}