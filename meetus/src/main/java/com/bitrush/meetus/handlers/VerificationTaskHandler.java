package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.VerificationFragment;
import com.bitrush.meetus.model.Token;
import com.google.gson.Gson;

/**
 * Created by vivek on 9/14/14.
 */
public class VerificationTaskHandler extends Handler {
    private VerificationFragment verificationFragment;
    @Override
    public void handleMessage(Message msg) {
        String message = (String)msg.obj;
        if (message.equals("")) {
            verificationFragment.verificationComplete(null);
        }
        Gson gson = new Gson();
        Token token = gson.fromJson(message, Token.class);
        if(token.getPhoneInfo() != null) {
            verificationFragment.verificationComplete(token);
        } else {
            verificationFragment.verificationComplete(null);
        }
    }

    public void setVerificationFragment(VerificationFragment verificationFragment) {
        this.verificationFragment = verificationFragment;
    }
}
