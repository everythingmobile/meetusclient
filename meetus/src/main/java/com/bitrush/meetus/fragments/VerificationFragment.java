package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.Token;
import com.bitrush.meetus.model.Verification;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.SharedPreferencesUtil;
import com.bitrush.meetus.util.StaticFunctions;

public class VerificationFragment extends Fragment {

    private static LoginRegisterActivity parentActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View finalView = inflater.inflate(R.layout.fragment_verification, container, false);
        setOnClickListener(finalView);
        return finalView;
    }

    private void setOnClickListener(final View view) {
        Button doneButton = (Button)view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Verification / Sanitization
                EditText phoneNumber = (EditText) view.findViewById(R.id.verificationString);
                String codeEntered = phoneNumber.getText().toString();
                if(codeEntered.equals("")) {
                    return;
                }
                verify(codeEntered);
            }
        });
    }

    private void verify(String codeEntered) {
        int countryCode = StaticFunctions.getUserCountryCode(parentActivity);
        long phoneNumber = StaticFunctions.getUserPhoneNumber(parentActivity);
        PhoneInfo phoneInfo = new PhoneInfo(countryCode, phoneNumber);
        Verification verification = new Verification(phoneInfo, codeEntered);
        Communicator.verify(verification, this);
    }

    public static VerificationFragment newInstance(LoginRegisterActivity loginRegisterActivity) {
        VerificationFragment.parentActivity = loginRegisterActivity;
        return new VerificationFragment();
    }

    public void verificationComplete(Token token) {
        if(token != null) {
            Toast.makeText(parentActivity.getApplicationContext(), "Verified", Toast.LENGTH_LONG).show();
            SharedPreferencesUtil.write(parentActivity, parentActivity.getResources().getString(R.string.preferenceUserName), token.getUserName());
            SharedPreferencesUtil.write(parentActivity, parentActivity.getResources().getString(R.string.preferenceToken), token.getAuthToken());
            parentActivity.displayFragment(LoginRegisterActivity.LoginRegisterFragments.SYNC);
            parentActivity.verificationDone();
        } else {
            Toast.makeText(parentActivity.getApplicationContext(), "Not Verified", Toast.LENGTH_LONG).show();
        }
    }
}
