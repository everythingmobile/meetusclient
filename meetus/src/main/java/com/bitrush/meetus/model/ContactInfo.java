package com.bitrush.meetus.model;

/**
 * Created by vivek on 1/4/15.
 */
public class ContactInfo {
    private UserInfo userInfo;
    private boolean isInvited;

    public ContactInfo(UserInfo userInfo, boolean isInvited) {
        this.userInfo = userInfo;
        this.isInvited = isInvited;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean isInvited) {
        this.isInvited = isInvited;
    }
}
