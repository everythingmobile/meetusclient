package com.bitrush.meetus;

import android.app.Application;
import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by vivek on 5/5/15.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(getApplicationContext(), getString(R.string.parseApplicationId), getString(R.string.parseClientKey));
        ParseInstallation.getCurrentInstallation();
    }
}
