package com.bitrush.meetus.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bitrush.meetus.MeetUpsActivity;
import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.R;
import com.bitrush.meetus.receivers.NotificationPushReceiver;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * @author ge3k on 27/1/15.
 */
public class MeetusNotificationService extends IntentService{
    private NotificationManager mNotificationManager;
    public static final int NOTIFICATION_ID = 3;

    public MeetusNotificationService() {
        super("MeetusNotificationService");

    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("GCM_TEST","IN the Handle Intent");
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {

            switch (messageType) {
                case GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR:
                    sendNotification("Send error: " + extras.toString());
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_DELETED:
                    sendNotification("Deleted messages on server: " + extras.toString());
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE:
                    String userDisplayMessage = extras.toString() + "has added to you to a meetup";
                    sendNotification("Received: " + userDisplayMessage);
                    break;
            }

            Log.i(MeetusConstants.APPTAG, "Received: " + extras.toString());
        }

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        NotificationPushReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MeetUpsActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Meetus")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
