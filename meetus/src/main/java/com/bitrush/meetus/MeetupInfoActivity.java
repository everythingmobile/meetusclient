package com.bitrush.meetus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.fragments.CommentsFragment;
import com.bitrush.meetus.fragments.InviteParticipantsFragment;
import com.bitrush.meetus.fragments.MeetUpDetailsFragment;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.model.UserInfo;

import java.util.List;

/**
 * Created by vivek on 2/25/15.
 */
public class MeetupInfoActivity extends ActionBarActivity {
    private StartMeetUpMessage startMeetUpMessage;
    private UserMeetUpRelationConfig userMeetUpRelationConfig;
    public static final String INTENT_PARAM_START_UP_MESSAGE = "startup_message";
    public static final String INTENT_PARAM_MEETUP_CONFIG = "meetup_config";
    public static final String INTENT_PARAM_MEETUP_ID = "meetup_id";
    public static final String INTENT_PARAM_COMMENTS = "comments";
    private Fragment currentFragment;
    private boolean addingMeetUp = false;
    private String meetUpId;
    private List<UserInfo> addedUsers;
    private List<UserInfo> removedUsers;

    public void setRemovedUsers(List<UserInfo> removedUsers) {
        this.removedUsers = removedUsers;
    }

    public void setAddedUsers(List<UserInfo> addedUsers) {
        this.addedUsers = addedUsers;
    }

    public List<UserInfo> getAddedUsers() {
        return addedUsers;
    }

    public List<UserInfo> getRemovedUsers() {
        return removedUsers;
    }

    private static enum MeetUpInfoFragment {
        MEET_DETAILS, INVITE, COMMENTS
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        startMeetUpMessage = intent.getParcelableExtra(INTENT_PARAM_START_UP_MESSAGE);
        if (startMeetUpMessage.getStartUpTime() == null) {
            addingMeetUp = true;
        }
        userMeetUpRelationConfig = intent.getParcelableExtra(INTENT_PARAM_MEETUP_CONFIG);
        meetUpId = intent.getStringExtra(INTENT_PARAM_MEETUP_ID);
        setContentView(R.layout.activity_meetup_info);
        getSupportActionBar().hide();
        setTitle("");
        if(intent.getBooleanExtra(INTENT_PARAM_COMMENTS, false)) {
            displayFragment(MeetUpInfoFragment.COMMENTS, null);
        } else {
            displayFragment(MeetUpInfoFragment.MEET_DETAILS, startMeetUpMessage);
        }
    }

    public void displayFragment(MeetUpInfoFragment addMeetUpFragment, Object parameter) {
        Fragment fragment = null;
        switch (addMeetUpFragment) {
            case MEET_DETAILS:
                fragment = MeetUpDetailsFragment.newInstance(this, (StartMeetUpMessage) parameter, userMeetUpRelationConfig);
                break;
            case INVITE:
                fragment = InviteParticipantsFragment.newInstance(this, (StartMeetUpMessage) parameter, userMeetUpRelationConfig);
                break;
            case COMMENTS:
                fragment = CommentsFragment.newInstance(this, meetUpId);
            default:
                break;
        }

        if (fragment != null) {
            fragment.setRetainInstance(true);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.placeholderFragment, fragment);
            fragmentTransaction.addToBackStack(null).commit();
            currentFragment = fragment;

        }
    }

    public void doneAddingMeetUp() {
        finish();
    }

    public void showInviteFragment() {
        displayFragment(MeetUpInfoFragment.INVITE, startMeetUpMessage);
    }

    public boolean isAddingMeetUp() {
        return addingMeetUp;
    }

    public void displayComments() {
        displayFragment(MeetUpInfoFragment.COMMENTS, null);
    }

    public String getMeetUpId() {
        return meetUpId;
    }
}
