package com.bitrush.meetus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.bitrush.meetus.R;
import com.bitrush.meetus.fragments.CommentsFragment;
import com.bitrush.meetus.model.UserAndUserComment;
import com.bitrush.meetus.util.StaticFunctions;

import java.util.List;

/**
 * Created by vivek on 4/18/15.
 */
public class CommentsAdapter extends ArrayAdapter<UserAndUserComment> {
    private final CommentsFragment parentFragment;
    private final Context context;
    private List<UserAndUserComment> userAndUserComments;

    static class CommentItemHolder {
        TextView commenterNameTV;
        TextView commentTimeTV;
        TextView commentBodyTV;
    }

    public CommentsAdapter(Context context, int resource, CommentsFragment parentFragment, List<UserAndUserComment> userAndUserComments) {
        super(context, resource);
        this.context = context;
        this.parentFragment = parentFragment;
        this.userAndUserComments = userAndUserComments;
    }

    @Override
    public UserAndUserComment getItem(int position) {
        return userAndUserComments.get(position);
    }

    @Override
    public int getPosition(UserAndUserComment item) {
        return userAndUserComments.indexOf(item);
    }

    @Override
    public int getCount() {
        return userAndUserComments.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommentItemHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.comment_list_item, null, false);

            viewHolder = new CommentItemHolder();
            viewHolder.commenterNameTV = (TextView) convertView.findViewById(R.id.commenterName);
            viewHolder.commentTimeTV = (TextView) convertView.findViewById(R.id.commentTime);
            viewHolder.commentBodyTV = (TextView) convertView.findViewById(R.id.commentBody);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CommentItemHolder) convertView.getTag();
        }
        viewHolder.commenterNameTV.setText(getItem(position).getUserInfo().getUserName());
        viewHolder.commentBodyTV.setText(getItem(position).getUserComment().getComment());
        if (getItem(position).getUserComment().getCommentTime() == null) {
            viewHolder.commentTimeTV.setText("Now");
        } else {
            viewHolder.commentTimeTV.setText(StaticFunctions.getDateWithCurrentTimeZone(getItem(position).getUserComment().getCommentTime()));
        }
        return convertView;
    }

    public void setUserAndUserComments(List<UserAndUserComment> userAndUserComments) {
        this.userAndUserComments = userAndUserComments;
        notifyDataSetInvalidated();
    }

    public void addComment(UserAndUserComment userAndUserComment, String time) {
        if (time.equals("Now")) {
            userAndUserComment.getUserComment().setCommentTime(null);
        }
        this.userAndUserComments.add(userAndUserComment);
        notifyDataSetChanged();
    }
}
