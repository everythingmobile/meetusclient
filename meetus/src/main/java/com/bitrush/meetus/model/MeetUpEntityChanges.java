package com.bitrush.meetus.model;

import java.util.Date;
import java.util.List;

/**
 * Created by vivek on 2/8/15.
 */
public class MeetUpEntityChanges {
    private String meetUpId;
    private List<UserInfo> addedUsers;
    private List<UserInfo> removedUsers;
    private Date startUpTime;

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }

    public List<UserInfo> getAddedUsers() {
        return addedUsers;
    }

    public void setAddedUsers(List<UserInfo> addedUsers) {
        this.addedUsers = addedUsers;
    }

    public List<UserInfo> getRemovedUsers() {
        return removedUsers;
    }

    public void setRemovedUsers(List<UserInfo> removedUsers) {
        this.removedUsers = removedUsers;
    }

    public Date getStartUpTime() {
        return startUpTime;
    }

    public void setStartUpTime(Date startUpTime) {
        this.startUpTime = startUpTime;
    }
}
