package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.R;
import com.bitrush.meetus.handlers.ContactDispatchHandler;
import com.bitrush.meetus.util.SharedPreferencesUtil;
import com.bitrush.meetus.util.UserContactsTask;

/**
 * @author ge3k on 20/9/14.
 */
public class SyncFragment extends Fragment{
    private static LoginRegisterActivity parentActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View finalView = inflater.inflate(R.layout.fragment_verification, container, false);
        return finalView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (SharedPreferencesUtil.read(parentActivity, MeetusConstants.FIRST_TIME_KEY).isEmpty()) {
            Log.i(MeetusConstants.APPTAG, "Trying to get all of the user's contacts");
            Handler contactsHandler = new ContactDispatchHandler(parentActivity);
            UserContactsTask userContactsTask = new UserContactsTask(parentActivity,contactsHandler);
            userContactsTask.execute();
        }
        SharedPreferencesUtil.write(parentActivity,MeetusConstants.FIRST_TIME_KEY,"YES");
    }

    public static SyncFragment newInstance(LoginRegisterActivity loginRegisterActivity) {
        SyncFragment.parentActivity = loginRegisterActivity;
        return new SyncFragment();
    }


}
