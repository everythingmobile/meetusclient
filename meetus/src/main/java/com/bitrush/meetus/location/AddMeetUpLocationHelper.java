package com.bitrush.meetus.location;

import android.app.Activity;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.fragments.AddMeetUpMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;

/**
 * Created by vivek on 10/25/14.
 */
public class AddMeetUpLocationHelper implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, MeetusConstants {

    private GoogleApiClient mLocationClient;
    private AddMeetUpMapFragment parentFragment;
    private LocationRequest locationRequest;
    private Location currentLocation;
    private Activity parentActivity;

    public AddMeetUpLocationHelper(AddMeetUpMapFragment fragment) {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(FAST_INTERVAL_CEILING_IN_MILLISECONDS);
        this.parentFragment = fragment;
        parentActivity = parentFragment.getParentActivity();
        mLocationClient = new GoogleApiClient.Builder(parentActivity)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationClient.connect();
    }

    public Location getLocation() {
        if (servicesConnected()) {
            return LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
        } else {
            Log.d(APPTAG,"Unable to find the location!!!");
            return null;
        }
    }

    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(parentActivity);
        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d(APPTAG, "Google play services available");
            return true;
        }
        return false;
    }

    private void startPeriodicUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, locationRequest, this);
    }

    @Override
    public void onConnected(Bundle bundle) {
        currentLocation = getLocation();
        parentFragment.startByZoomingToCurrentLocation();
        startPeriodicUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(parentActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.d(APPTAG, "An error occurred when connecting to location services.", e);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("Connection has been suspended", APPTAG);
    }
}
