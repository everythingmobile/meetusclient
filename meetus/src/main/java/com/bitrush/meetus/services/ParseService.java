package com.bitrush.meetus.services;

import android.app.Activity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.util.SharedPreferencesUtil;
import com.bitrush.meetus.util.StaticFunctions;
import com.parse.ParseInstallation;

/**
 * Created by vivek on 5/5/15.
 */
public class ParseService {
    private static Activity parentActivity;
    private ParseInstallation currentInstallation;
    private ParseService() {

    }

    public static ParseService getInstance(Activity parentActivity) {
        ParseService.parentActivity = parentActivity;
        return new ParseService();
    }

    public ParseInstallation setUpAndGetInstallation() {
        if (currentInstallation == null) {
            currentInstallation = ParseInstallation.getCurrentInstallation();
        }
        if (SharedPreferencesUtil.read(parentActivity, parentActivity.getString(R.string.parsePhoneNumber)).equals("")) {
            String parsePhoneNumber = StaticFunctions.getUserCountryCode(parentActivity) + "_" + StaticFunctions.getUserPhoneNumber(parentActivity);
            currentInstallation.add(parentActivity.getString(R.string.parsePhoneNumber), parsePhoneNumber);
            currentInstallation.saveInBackground();
            SharedPreferencesUtil.write(parentActivity, parentActivity.getString(R.string.parsePhoneNumber), parsePhoneNumber);
        }
        return currentInstallation;
    }
}
