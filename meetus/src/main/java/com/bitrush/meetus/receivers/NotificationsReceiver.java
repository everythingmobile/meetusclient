package com.bitrush.meetus.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.bitrush.meetus.DummySpinnerActivity;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 5/7/15.
 */
public class NotificationsReceiver extends ParsePushBroadcastReceiver {

    private static final String APPTAG = "com.bitrush.meetus";

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        ParseAnalytics.trackAppOpenedInBackground(intent);
        try {
            notificationRouter(context, new JSONObject(intent.getStringExtra("com.parse.Data")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notificationRouter(Context context, JSONObject input) {
        String alert;
        try {
            alert = input.getString("alert");
        } catch (JSONException e) {
            return;
        }
        if (alert.contains("commented on")) {
            try {
                showComments(context, input.getString("meetUpId"));
            } catch (JSONException e) {
                Log.i(APPTAG, "Cant parse comments object");
            }
        } else if (alert.contains("added to")) {
            try {
                showMeetUpDetails(context, input.getString("meetUpId"));
            } catch (JSONException e) {
                Log.i(APPTAG, "Cant parse meetUp object");
            }
        }
    }

    private void showMeetUpDetails(Context context, String meetUpId) {

        String extraString = DummySpinnerActivity.START_MEETUP_DETAILS + "|" + meetUpId;

        Intent activityIntent = new Intent(context, DummySpinnerActivity.class);
        activityIntent.putExtra(DummySpinnerActivity.EXTRA_STRING, extraString);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(activityIntent);
    }

    private void showComments(Context context, String meetUpId) {
        String extraString = DummySpinnerActivity.START_COMMENTS + "|" + meetUpId;

        Intent activityIntent = new Intent(context, DummySpinnerActivity.class);
        activityIntent.putExtra(DummySpinnerActivity.EXTRA_STRING, extraString);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(activityIntent);
    }
}
