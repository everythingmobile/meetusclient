package com.bitrush.meetus.model;

/**
 * Created by vivek on 9/13/14.
 */
public class RegistrationVerficationMessage {
    private PhoneInfo phoneInfo;
    private Boolean registered;

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public Boolean getRegistered() {
        return registered;
    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }
}
