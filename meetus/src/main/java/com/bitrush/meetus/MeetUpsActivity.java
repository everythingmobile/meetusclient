package com.bitrush.meetus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.*;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.bitrush.meetus.adapters.LeftNavDrawerAdapter;
import com.bitrush.meetus.fragments.*;
import com.bitrush.meetus.services.ParseService;
import com.bitrush.meetus.util.PlayServiceHelper;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.IOException;

/**
 * Created by vivek on 9/20/14.
 */
public class MeetUpsActivity extends ActionBarActivity implements ContactFragment.OnFragmentInteractionListener{

    //TODO: Should we add a new logout button ?
    private Fragment currentFragment;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView leftDrawer;
    private CharSequence mTitle;
    private LeftNavDrawerAdapter leftDrawerAdapter;
    private GoogleCloudMessaging gcm;
    private String regid;
    private Context context;
    private static final int PICK_CONTACT_REQUEST = 1;  // The request code

    @Override
    public void onFragmentInteraction(String id) {
        Toast.makeText(this, "Touched an item", Toast.LENGTH_SHORT).show();
    }

    public static enum MeetUpsActivityFragments {
        FEED, CONTACT,MY_MEETUPS
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            selectItem(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ParseService.getInstance(this).setUpAndGetInstallation();
        setContentView(R.layout.activity_meetup);
        context = this;

        if (PlayServiceHelper.checkPlayServices(this)) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(this);
            Log.i("GCM_TEST", regid);
            if (regid.isEmpty()) {
                registerInBackground();
            }
            displayFragment(MeetUpsActivityFragments.FEED);
            setUpNavDrawer();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register("19580527909"); //TODO get one
                    msg = "Device registered, registration ID=" + regid;
                    sendRegistrationIdToBackend();
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    //TODO perform  exponential back-off and re-attempt.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //mDisplay.append(msg + "\n");
                //TODO:
            }

        }.execute(null, null, null);

    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(MeetusConstants.APPTAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MeetusConstants.PROPERTY_REG_ID, regId);
        editor.putInt(MeetusConstants.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + number));
                sendIntent.putExtra("sms_body", "Checkout the awesome new Meetus app today.");
                startActivity(sendIntent);
            }
        }
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }

    private void setUpNavDrawer() {
        leftDrawer = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mTitle = getTitle();
        leftDrawer = (ListView) findViewById(R.id.left_drawer);

        leftDrawerAdapter = new LeftNavDrawerAdapter(getApplicationContext(), R.layout.left_nav_item);
        leftDrawer.setAdapter(leftDrawerAdapter);
        leftDrawer.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void selectItem(int position) {
        switch(position) {
            case 1:
                displayFragment(MeetUpsActivityFragments.CONTACT);
                break;
            case 0:
                displayFragment(MeetUpsActivityFragments.FEED);
                break;
            case 2:
                displayFragment(MeetUpsActivityFragments.MY_MEETUPS);
                break;
        }
        //Toast.makeText(getApplicationContext(), "Clicked", Toast.LENGTH_LONG).show();
    }

    public void displayFragment(MeetUpsActivityFragments meetUpsActivityFragments)
    {
        Fragment fragment = null;
        switch (meetUpsActivityFragments) {
            case FEED:
                fragment = FeedDisplayFragment.newInstance(this);
                break;
            case CONTACT:
                fragment = ContactFragment.newInstance(this);
                break;
            case MY_MEETUPS:
                fragment = CurrentMeetupsFragment.newInstance(this);
                break;
        }

        if(fragment != null)
        {
            fragment.setRetainInstance(true);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.meetUpFragment, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            currentFragment = fragment;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.meetup_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_meetUp_add:
                Toast.makeText(getApplicationContext(), "Adding MeetUp", Toast.LENGTH_LONG).show();
                addMeetUp();
                return true;
            case R.id.action_meetp_start:
                return true;
            case R.id.action_invite_friends:
                inviteFriends();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addMeetUp() {
        Intent intent = new Intent(getApplicationContext(), AddMeetUpActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void inviteFriends() {
        pickContact();
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(MeetusConstants.PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(MeetusConstants.APPTAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(MeetusConstants.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(MeetusConstants.APPTAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(LoginRegisterActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void sendRegistrationIdToBackend() {
        // Your implementation here.
        //TODO
    }

}
