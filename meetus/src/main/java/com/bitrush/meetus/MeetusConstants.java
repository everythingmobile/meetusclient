package com.bitrush.meetus;

/**
 * @author ge3k on 14/9/14.
 */
public interface MeetusConstants {
    public static final String APPTAG = "Meetus";

    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final int MILLISECONDS_PER_SECOND = 1000;
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    public static final int FAST_CEILING_IN_SECONDS = 1;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * FAST_CEILING_IN_SECONDS;
    public static final float SMALLEST_DISPLACEMENT = 100.0f;

    final String LOCATION_CHANGED_SERIALIZABLE_KEY = "userLocation";
    final int MESSAGE_KEY_LOCATION = 986;
    final double OFFSET_CALCULATION_INIT_DIFF = 1.0;
    final float OFFSET_CALCULATION_ACCURACY = 0.01f;
    final float MEETUS_RADIUS = 100.00f;
    final long MAX_TIMEOUT_WEBSOCKET = 25000; //2.5 seconds
    final String KEY_FRIEND_DETAIL_LIST = "FRIEND_ON_MEETUS";
    final String FIRST_TIME_KEY = "FIRST_RUN";
    String PROPERTY_REG_ID = "com.bitrush.reg_id";
    String PROPERTY_APP_VERSION = "com.bitrush.app_version";
    final static String USER_LOCATION_BROADCAST = "com.bitrush.user.location.changed";
    final static String LOCATION_REGULAR_MESSAGE_KEY = "com.birush.loaction.regular.key";
}
