package com.bitrush.meetus.model;

/**
 * Created by vivek on 12/13/14.
 */
public class Verification {
    private PhoneInfo phoneInfo;
    private String verificationString;

    public Verification(PhoneInfo phoneInfo, String verificationString) {
        this.phoneInfo = phoneInfo;
        this.verificationString = verificationString;
    }

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public String getVerificationString() {
        return verificationString;
    }

    public void setVerificationString(String verificationString) {
        this.verificationString = verificationString;
    }
}
