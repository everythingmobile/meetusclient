package com.bitrush.meetus.handlers;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.contentprovider.MeetusContactProvider;
import com.bitrush.meetus.database.ContactSqlTableHelper;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.model.UserInfo;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * @author ge3k on 26/10/14.
 */
public class UserInfoAcceptHandler extends Handler {

    private LoginRegisterActivity parentActivity;

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if(msg.obj == null || msg.obj.equals("")) {
            return;
        }
        Gson gson = new Gson();
        try {
            List<UserInfo> userInfos = gson.fromJson((String)msg.obj, new TypeToken<List<UserInfo>>(){}.getType());
            Log.i(MeetusConstants.APPTAG,"Inserting into db records : "+ userInfos);
            ContentResolver resolver = parentActivity.getContentResolver();
            String stringUri = MeetusContactProvider.CONTENT_URI.toString();
            Uri sqliteUri = Uri.parse(stringUri);
            for (UserInfo userInfo : userInfos) {
                insertContact(resolver, sqliteUri, userInfo);
            }
        } catch (JsonSyntaxException e) {
            Log.e(MeetusConstants.APPTAG,"ERROR Parsing JSON",e);
        }
    }

    private void insertContact(ContentResolver resolver, Uri sqliteUri, UserInfo userInfo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContactSqlTableHelper.USER_NAME,userInfo.getUserName());
        contentValues.put(ContactSqlTableHelper.COUNTRY_CODE,String.valueOf(userInfo.getPhoneInfo().getCountryCode()));
        contentValues.put(ContactSqlTableHelper.PHONE_NUM,String.valueOf(userInfo.getPhoneInfo().getPhoneNumber()));
        contentValues.put(ContactSqlTableHelper.EMAIL_ADD,userInfo.getEmailId());

        Uri insertUri = resolver.insert(sqliteUri,contentValues);
        Log.i(MeetusConstants.APPTAG, "Successfully inserted the item with uri : " + insertUri);
    }

    public LoginRegisterActivity getParentActivity() {
        return parentActivity;
    }

    public void setParentActivity(LoginRegisterActivity parentActivity) {
        this.parentActivity = parentActivity;
    }
}
