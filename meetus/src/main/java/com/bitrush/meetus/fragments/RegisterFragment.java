package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.CountriesDialog;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.SharedPreferencesUtil;

public class RegisterFragment extends Fragment {

    private static LoginRegisterActivity parentActivity;
    private static String finalPhoneNumber;
    private static int finalCountryCode;
    private Spinner spinner;
    private String[] countryCodes;
    private CountriesDialog countriesDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        View finalView = inflater.inflate(R.layout.fragment_register, container, false);
        countryCodes = getResources().getStringArray(R.array.CountryNames);
        spinner = (Spinner) finalView.findViewById(R.id.country_list);
        setBuilder();
        setOnClickListeners(finalView);
        return finalView;
    }

    private void setBuilder() {
        countriesDialog = new CountriesDialog(parentActivity);
        countriesDialog.initializeBuilder(countryCodes, spinner);
    }

    private void setOnClickListeners(final View view) {
        Button doneButton = (Button)view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText phoneNumber = (EditText) view.findViewById(R.id.registerFragmentPhoneNUmber);
                String phoneNumberEntered = phoneNumber.getText().toString();
                EditText name = (EditText) view.findViewById(R.id.registerName);
                String nameEntered = name.getText().toString();
                EditText email = (EditText) view.findViewById(R.id.registerEmail);
                String emailEntered = email.getText().toString();
                if(phoneNumberEntered.equals("")) {
                    return;
                }
                finalPhoneNumber = phoneNumber.getText().toString();
                finalCountryCode = countriesDialog.getCurrentCountryCode();
                PhoneInfo phoneInfo = new PhoneInfo(finalCountryCode, Long.parseLong(finalPhoneNumber));
                UserInfo userInfo = new UserInfo();
                userInfo.setEmailId(emailEntered);
                userInfo.setUserName(nameEntered);
                finalPhoneNumber = phoneNumberEntered;
                userInfo.setPhoneInfo(phoneInfo);

                userAdd(userInfo);
            }
        });
    }

    private void userAdd(UserInfo userInfo) {
        Communicator.userAdd(userInfo, this);
    }

    public static RegisterFragment newInstance(LoginRegisterActivity loginRegisterActivity) {
        RegisterFragment.parentActivity = loginRegisterActivity;
        return new RegisterFragment();
    }

    public void userAdded() {
        SharedPreferencesUtil.write(parentActivity, parentActivity.getResources().getString(R.string.preferenceCountryCode), Integer.toString(finalCountryCode));
        SharedPreferencesUtil.write(parentActivity, parentActivity.getResources().getString(R.string.preferencePhoneNumber), finalPhoneNumber);
        parentActivity.displayFragment(LoginRegisterActivity.LoginRegisterFragments.VERIFY);
    }
}
