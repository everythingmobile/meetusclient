package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.bitrush.meetus.AddMeetUpActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.PlacesAutoCompleteAdapter;
import com.bitrush.meetus.model.Location;
import com.bitrush.meetus.util.Communicator;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class PlacesAutoCompleteFragment extends Fragment {

    private View finalView;
    private static AddMeetUpActivity parentActivity;
    private PlacesAutoCompleteAdapter placesAutoCompleteAdapter;
    private String currentQuery;

    public PlacesAutoCompleteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        finalView = inflater.inflate(R.layout.places_autocomplete, container, false);
        setAdapters();
        setListeners();
        return finalView;
    }

    private void setListeners() {
        ListView listView = (ListView) finalView.findViewById(R.id.placesListView);
        final PlacesAutoCompleteFragment thisFragment = this;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                currentQuery = textView.getText().toString();
                Communicator.getLocationFromGivenAddress((String)view.getTag(), thisFragment);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.showSearchBox();
    }

    public void markMeetUpLocation(Location location) {
        location.setLocationAddress(currentQuery);
        parentActivity.setCurrentMeetUpLocationAndMarkItOnMap(location);
    }

    private void setAdapters() {
        EditText editText = (EditText) parentActivity.findViewById(R.id.autoComplete);
        ListView listView = (ListView) finalView.findViewById(R.id.placesListView);
        placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(parentActivity, R.layout.auto_complete_list_item);
        listView.setAdapter(placesAutoCompleteAdapter);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesAutoCompleteAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static PlacesAutoCompleteFragment newInstance(AddMeetUpActivity parentActivity) {
        PlacesAutoCompleteFragment.parentActivity = parentActivity;
        return new PlacesAutoCompleteFragment();
    }
}
