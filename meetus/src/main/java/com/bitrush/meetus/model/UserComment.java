package com.bitrush.meetus.model;

import java.util.Date;

public class UserComment {
    private int commentId;
    private String meetUpId;
    private PhoneInfo phoneInfo;
    private String comment;
    private Date commentTime;

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    @Override
    public int hashCode() {
        return commentId+meetUpId.hashCode()+phoneInfo.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        UserComment userComment = (UserComment) obj;
        return this.getCommentId() == userComment.getCommentId() && this.phoneInfo.equals(userComment.getPhoneInfo());
    }
}