package com.bitrush.meetus.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import com.bitrush.meetus.MeetupInfoActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.CommentsAdapter;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserAndUserComment;
import com.bitrush.meetus.model.UserComment;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.StaticFunctions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommentsFragment extends Fragment {
    private static MeetupInfoActivity parentActivity;
    private static String meetUpId;
    private View parentView;
    private ListView listView;
    private CommentsAdapter commentsAdapter;
    private ImageView sendButton;
    private EditText commentBody;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_comments, container, false);
        setViews();
        setAdapters();
        setListeners();
        PhoneInfo phoneInfo = StaticFunctions.getCurrentUserInfo(parentActivity).getPhoneInfo();
        Communicator.getComments(meetUpId, phoneInfo, this);
        return parentView;
    }

    private void setViews() {
        listView = (ListView) parentView.findViewById(R.id.commentsListView);
        sendButton = (ImageView) parentView.findViewById(R.id.sendButton);
        commentBody = (EditText) parentView.findViewById(R.id.commentBody);
    }

    private void setListeners() {
        final CommentsFragment thisF = this;
        listView = (ListView) parentView.findViewById(R.id.commentsListView);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = commentBody.getText().toString();
                if (!comment.equals("")) {
                    addAndSendComment(comment, thisF);
                }
            }
        });
    }

    private void addAndSendComment(String comment, CommentsFragment thisF) {
        UserAndUserComment userAndUserComment = new UserAndUserComment();
        UserComment userComment = new UserComment();
        userComment.setComment(comment);
        userComment.setCommentId(0);
        userComment.setCommentTime(new Date());
        userComment.setMeetUpId(meetUpId);
        userComment.setPhoneInfo(StaticFunctions.getCurrentUserInfo(parentActivity).getPhoneInfo());
        userAndUserComment.setUserInfo(StaticFunctions.getCurrentUserInfo(parentActivity));
        userAndUserComment.setUserComment(userComment);
        InputMethodManager imm = (InputMethodManager) parentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(parentView.getWindowToken(), 0);
        commentBody.setText("");
        Communicator.addComment(userComment, thisF);
        commentsAdapter.addComment(userAndUserComment, "Now");
    }

    private void setAdapters() {
        commentsAdapter = new CommentsAdapter(parentActivity, R.layout.comment_list_item, this, new ArrayList<UserAndUserComment>());
        listView.setAdapter(commentsAdapter);
    }

    public static CommentsFragment newInstance(MeetupInfoActivity meetupInfoActivity, String meetUpId) {
        CommentsFragment commentsFragment = new CommentsFragment();
        CommentsFragment.meetUpId = meetUpId;
        CommentsFragment.parentActivity = meetupInfoActivity;
        return commentsFragment;
    }

    public void setComments(List<UserAndUserComment> userAndUserComments) {
        commentsAdapter.setUserAndUserComments(userAndUserComments);
    }
}
