package com.bitrush.meetus.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ge3k on 25/10/14.
 */
public class ContactSqlTableHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static final String DB_NAME="ContactsOnMeetus";
    public static final String TABLE_CONTACTS = "contacts";

    public static final String COLUMN_ID = "_id";
    public static final String USER_NAME = "name";
    public static final String EMAIL_ADD = "email";
    public static final String PHONE_NUM = "phone_num";
    public static final String COUNTRY_CODE = "country_code";
    private static final String primaryKeyWhereClause = PHONE_NUM + "=? AND " + COUNTRY_CODE + "=?";

    public ContactSqlTableHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //TODO: aish change this to have the country code too
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + COLUMN_ID + " integer primary key autoincrement, " + COUNTRY_CODE + " STRING not null, "
                + PHONE_NUM + " STRING UNIQUE not null," + USER_NAME + " TEXT not null,"
                + EMAIL_ADD + " TEXT" + ");";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(db);
    }

    public void addUserInfo(UserInfo contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_NAME, contact.getUserName());
        values.put(PHONE_NUM, String.valueOf(contact.getPhoneInfo().getPhoneNumber()));
        values.put(COUNTRY_CODE, String.valueOf(contact.getPhoneInfo().getCountryCode()));
        values.put(EMAIL_ADD, contact.getEmailId());
        db.insert(TABLE_CONTACTS, null, values);
        db.close();
    }

    public UserInfo getUserByPhoneInfo(PhoneInfo phoneInfo) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] {COUNTRY_CODE, PHONE_NUM, USER_NAME, EMAIL_ADD }, primaryKeyWhereClause ,
                new String[] { String.valueOf(phoneInfo.getPhoneNumber()), String.valueOf(phoneInfo.getCountryCode()) }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        if (cursor == null) {
            return new UserInfo();//todo change this
        }
        UserInfo ret = new UserInfo();
        ret.setPhoneInfo(new PhoneInfo(Integer.parseInt(cursor.getString(0)), Long.parseLong(cursor.getString(1))));
        ret.setUserName(cursor.getString(2));
        ret.setEmailId(cursor.getString(3));
        return ret;
    }

    public List<UserInfo> getAllContacts() {
        List<UserInfo> contactList = new ArrayList<UserInfo>();
        String selectQuery = "SELECT " + COUNTRY_CODE + ", " + PHONE_NUM + ", " + USER_NAME + ", " + EMAIL_ADD +" FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                UserInfo userInfo = new UserInfo();
                PhoneInfo phoneInfo = new PhoneInfo();
                phoneInfo.setCountryCode(Integer.parseInt(cursor.getString(0)));
                phoneInfo.setPhoneNumber(Long.parseLong(cursor.getString(1)));
                userInfo.setUserName(cursor.getString(2));
                userInfo.setPhoneInfo(phoneInfo);
                userInfo.setEmailId(cursor.getString(3));
                contactList.add(userInfo);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public int updateContact(UserInfo userInfo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_NAME, userInfo.getUserName());
        values.put(COUNTRY_CODE, userInfo.getPhoneInfo().getCountryCode());
        values.put(PHONE_NUM, userInfo.getPhoneInfo().getPhoneNumber());
        values.put(EMAIL_ADD, userInfo.getEmailId());

        return db.update(TABLE_CONTACTS, values, primaryKeyWhereClause,
                new String[] { String.valueOf(userInfo.getPhoneInfo().getPhoneNumber()), String.valueOf(userInfo.getPhoneInfo().getCountryCode()) });
    }

    public void deleteContact(UserInfo userInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, primaryKeyWhereClause,
                new String[] { String.valueOf(userInfo.getPhoneInfo().getPhoneNumber()), String.valueOf(userInfo.getPhoneInfo().getCountryCode()) });
        db.close();
    }

    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }


}
