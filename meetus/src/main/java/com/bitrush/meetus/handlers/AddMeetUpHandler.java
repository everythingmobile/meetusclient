package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.MeetUpDetailsFragment;

/**
 * Created by vivek on 1/18/15.
 */
public class AddMeetUpHandler extends Handler {

    private MeetUpDetailsFragment listener;

    public AddMeetUpHandler(MeetUpDetailsFragment listener) {
        this.listener = listener;
    }

    @Override
    public void handleMessage(Message msg) {
        String meetUpId = (String) msg.obj;
        listener.doneAddingMeetUp(meetUpId);
    }
}
