package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import android.widget.TextView;
import com.bitrush.meetus.MeetupInfoActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.ContactsCheckedListViewAdapter;
import com.bitrush.meetus.adapters.GoingCheckedListViewAdapter;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.database.ContactSqlTableHelper;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

public class InviteParticipantsFragment extends Fragment {

    public static MeetupInfoActivity parentActivity;
    private static UserMeetUpRelationConfig config;
    private StartMeetUpMessage startMeetUpMessage;
    private View currentView;
    private ListView goingLV;
    private ListView contactsLV;
    private TextView doneButton;
    private ContactSqlTableHelper contactSqlTableHelper;
    private GoingCheckedListViewAdapter goingCheckedListViewAdapter;
    private ContactsCheckedListViewAdapter contactsCheckedListViewAdapter;
    public List<UserInfo> participants;
    private List<UserInfo> addedUsers;
    private List<UserInfo> removedUsers;

    public InviteParticipantsFragment() {
    }

    public static InviteParticipantsFragment newInstance(MeetupInfoActivity parentActivity, StartMeetUpMessage startMeetUpMessage,
                                                         UserMeetUpRelationConfig config) {
        InviteParticipantsFragment.config = config;
        InviteParticipantsFragment.parentActivity = parentActivity;
        InviteParticipantsFragment inviteParticipantsFragment = new InviteParticipantsFragment();
        inviteParticipantsFragment.startMeetUpMessage = startMeetUpMessage;
        inviteParticipantsFragment.addedUsers = new ArrayList<>();
        inviteParticipantsFragment.removedUsers = new ArrayList<>();
        return inviteParticipantsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        currentView = inflater.inflate(R.layout.fragment_invite_participants, container, false);
        if(startMeetUpMessage.getInvitees() == null) {
            startMeetUpMessage.setInvitees(new ArrayList<UserInfo>());
        }
        setViews();
        setAdapters();
        setListeners();
        return currentView;
    }

    private void setAdapters() {
        contactSqlTableHelper = new ContactSqlTableHelper(parentActivity);
        if (startMeetUpMessage.getInvitees() == null) {
            startMeetUpMessage.setInvitees(new ArrayList<UserInfo>());
        }
        participants = startMeetUpMessage.getInvitees();
        goingCheckedListViewAdapter = new GoingCheckedListViewAdapter(parentActivity, R.layout.invite_list_item, participants, goingLV, this, config);
        contactsCheckedListViewAdapter = new ContactsCheckedListViewAdapter(parentActivity, R.layout.invite_list_item, participants, contactSqlTableHelper.getAllContacts(), contactsLV, this, config);
        goingLV.setAdapter(goingCheckedListViewAdapter);
        contactsLV.setAdapter(contactsCheckedListViewAdapter);
        GoingCheckedListViewAdapter.setListViewHeightBasedOnChildren(goingLV);
        GoingCheckedListViewAdapter.setListViewHeightBasedOnChildren(contactsLV);
    }

    private void setViews() {
        goingLV = (ListView) currentView.findViewById(R.id.goingLV);
        contactsLV = (ListView) currentView.findViewById(R.id.contactsLV);
        doneButton = (TextView) currentView.findViewById(R.id.doneButton);
    }

    private void setListeners() {
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMeetUpMessage.setInvitees(participants);
                parentActivity.setAddedUsers(addedUsers);
                parentActivity.setRemovedUsers(removedUsers);
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }


    public void participantsChanged(boolean changeFromContacts) {
        if(goingCheckedListViewAdapter != null) {
            goingCheckedListViewAdapter.participantsChanged();
        }
        if(contactsCheckedListViewAdapter != null && !changeFromContacts) {
            contactsCheckedListViewAdapter.participantsChanged();
        }
    }

    public void addedUser(UserInfo userInfo) {
        if (removedUsers.indexOf(userInfo) != -1) {
            removedUsers.remove(userInfo);
        } else {
            addedUsers.add(userInfo);
        }
    }

    public void removedUser(UserInfo userInfo) {
        if (addedUsers.indexOf(userInfo) != -1) {
            addedUsers.remove(userInfo);
        } else {
            removedUsers.add(userInfo);
        }
    }
}
