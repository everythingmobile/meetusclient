package com.bitrush.meetus.util;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.widget.Toast;

import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.MeetUpRecycleAdapter;
import com.bitrush.meetus.model.MeetUpEntity;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ge3k on 16/05/15.
 */
public  final class PullToRefreshHelper {

    private final Activity parentActivity;

    public PullToRefreshHelper(Activity parentActivity) {
        this.parentActivity = parentActivity;
    }


    public Activity getParentActivity() {
        return parentActivity;
    }

    public void performAsyncLoad(final MeetUpRecycleAdapter meetUpsAdapter, final SwipeRefreshLayout swipeRefreshLayout, final String meetupsType) {
        //well looks like its already in background.
        new AsyncTask<Void, Void, Void>() {
            private String errorMessage = null;

            @Override
            protected Void doInBackground(final Void... params) {
                String methodName = "getUserFeed";
                String phoneNumber = SharedPreferencesUtil.
                        read(parentActivity, parentActivity.getResources().getString(R.string.preferencePhoneNumber));
                String countryCode = SharedPreferencesUtil.
                        read(parentActivity, parentActivity.getResources().getString(R.string.preferenceCountryCode));
                if (meetupsType != null && !meetupsType.isEmpty()) {
                    methodName =  meetupsType;
                }
                Uri.Builder uriBuilder = Uri.parse("http://" + Communicator.mServerAddress).buildUpon()
                        .appendPath("MeetusServer")
                        .appendPath(methodName).appendQueryParameter("pageNum", "1")
                        .appendQueryParameter("phoneNumber", phoneNumber)
                        .appendQueryParameter("countryCode", countryCode);
                Uri refreshUri = uriBuilder.build();
                Log.i(MeetusConstants.APPTAG, " The URL is " + refreshUri);
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(refreshUri.toString()).get().build();

                String serverResponseString = null;
                try {
                    Response responseMessage = client.newCall(request).execute();
                    serverResponseString = responseMessage.body().string();
                    Log.i(MeetusConstants.APPTAG, "^^^^^^^ Response received from the server ^^^^^^^");
                    Log.i(MeetusConstants.APPTAG, "Response is: " + serverResponseString);
                } catch (IOException e) {
                    errorMessage = "Unable to communicate with the server";
                    Log.e(MeetusConstants.APPTAG, errorMessage + " Error: ", e);
                }
                Gson gson = StaticFunctions.getGsonWithDateRegister();
                try {
                    Type listType = new TypeToken<ArrayList<MeetUpEntity>>() {
                    }.getType();
                    List<MeetUpEntity> meetUpEntities = gson.fromJson(serverResponseString, listType);
                    meetUpsAdapter.setMeetUps(meetUpEntities);
                } catch (JsonSyntaxException e) {
                    errorMessage = "Error communicating with the server.";
                    Log.e(MeetusConstants.APPTAG, "Unable to parse JSON. Error is :", e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Void result) {
                Log.i(MeetusConstants.APPTAG, "^^^^^^ Done updating ^^^^");
                if (errorMessage != null) {
                    Toast.makeText(parentActivity, errorMessage, Toast.LENGTH_SHORT).show();
                }
                swipeRefreshLayout.setRefreshing(false);
                meetUpsAdapter.notifyDataSetChanged();
            }
        }.execute();

    }
}
