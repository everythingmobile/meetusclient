package com.bitrush.meetus;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import com.bitrush.meetus.model.MeetUpPollResponse;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.StaticFunctions;

/**
 * Created by vivek on 5/17/15.
 */
public class DummySpinnerActivity extends Activity {
    public static final String EXTRA_STRING = "dummy_activity_extra_string";
    public static final String START_MEETUP_DETAILS = "start_meetup_details";
    public static final String START_COMMENTS = "start_comments";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProgressDialog.show(this, "Loading", "Wait while loading...");
        String routingString = getIntent().getStringExtra(EXTRA_STRING);
        routeAndInitiateRequest(routingString);
    }

    private void routeAndInitiateRequest(String routingString) {
        String[] info = routingString.split("\\|");

        switch (info[0]) {
            case START_MEETUP_DETAILS:
                Communicator.getMeetUpDetails(info[1], StaticFunctions.getCurrentUserInfo(this).getPhoneInfo(), this);
                break;
            case START_COMMENTS:
                Communicator.getMeetUpDetailsAndComments(info[1], StaticFunctions.getCurrentUserInfo(this).getPhoneInfo(), this);
                break;
        }
    }

    public void startMeetUpActivity(MeetUpPollResponse meetUpPollResponse, boolean comments, String meetUpId) {
        Intent activityIntent = new Intent(this, MeetupInfoActivity.class);
        activityIntent.putExtra(MeetupInfoActivity.INTENT_PARAM_MEETUP_CONFIG, meetUpPollResponse.getConfig());
        activityIntent.putExtra(MeetupInfoActivity.INTENT_PARAM_START_UP_MESSAGE, meetUpPollResponse.getStartMeetUpMessage());
        activityIntent.putExtra(MeetupInfoActivity.INTENT_PARAM_MEETUP_ID, meetUpId);
        activityIntent.putExtra(MeetupInfoActivity.INTENT_PARAM_COMMENTS, comments);
        startActivity(activityIntent);
        finish();
    }
}
