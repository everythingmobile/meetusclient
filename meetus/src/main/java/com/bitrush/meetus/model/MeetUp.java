package com.bitrush.meetus.model;

/**
 * Created by vivek on 7/16/14.
 */
public class MeetUp {
    private String meetUpId;
    private PhoneInfo participantPhoneInfo;
    private Long startUpTime;
    private Boolean isParticipantAdmin;
    private Location meetUpLocation;

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }

    public PhoneInfo getParticipantPhoneInfo() {
        return participantPhoneInfo;
    }

    public void setParticipantPhoneInfo(PhoneInfo participantPhoneInfo) {
        this.participantPhoneInfo = participantPhoneInfo;
    }

    public Long getStartUpTime() {
        return startUpTime;
    }

    public void setStartUpTime(Long startUpTime) {
        this.startUpTime = startUpTime;
    }

    public Boolean getIsParticipantAdmin() {
        return isParticipantAdmin;
    }

    public void setIsParticipantAdmin(Boolean isParticipantAdmin) {
        this.isParticipantAdmin = isParticipantAdmin;
    }

    public Location getMeetUpLocation() {
        return meetUpLocation;
    }

    public void setMeetUpLocation(Location meetUpLocation) {
        this.meetUpLocation = meetUpLocation;
    }
}
