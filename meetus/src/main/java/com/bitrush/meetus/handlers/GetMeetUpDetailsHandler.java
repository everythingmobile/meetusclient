package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.DummySpinnerActivity;
import com.bitrush.meetus.model.MeetUpPollResponse;
import com.google.gson.Gson;

/**
 * Created by vivek on 5/17/15.
 */
public class GetMeetUpDetailsHandler extends Handler {
    private final String meetUpId;
    private DummySpinnerActivity dummySpinnerActivity;
    private boolean comments;

    public GetMeetUpDetailsHandler(DummySpinnerActivity dummySpinnerActivity, String meetUpId) {
        this.dummySpinnerActivity = dummySpinnerActivity;
        this.meetUpId = meetUpId;
    }

    @Override
    public void handleMessage(Message msg) {
        String response = (String)msg.obj;
        if (response == null || response.equals("")) {
            return;
        }
        Gson gson = new Gson();
        dummySpinnerActivity.startMeetUpActivity(gson.fromJson(response, MeetUpPollResponse.class), comments, meetUpId);
    }

    public void setCommentsTrue() {
        this.comments = true;
    }
}
