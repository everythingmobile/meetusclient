package com.bitrush.meetus.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Spinner;
import com.bitrush.meetus.model.CountryCode;

import java.lang.reflect.Array;

/**
 * Created by vivek on 4/30/15.
 */
public class CountriesDialog extends AlertDialog.Builder {

    private final Context context;
    private CountriesListAdapter countriesAdapter;
    private Spinner spinner;
    private CountryCode pickedCountry;

    public CountriesDialog(Context context) {
        super(context);
        this.context = context;
    }

    public void initializeBuilder(String[] countryCodes, Spinner spinner) {
        this.spinner = spinner;
        setTitle("Select your country");
        countriesAdapter = new CountriesListAdapter(context, countryCodes);
        String currentCountry = (String) Array.get(countryCodes, countriesAdapter.getCurrentIsdPosition());
        showStringInSpinner(currentCountry);

        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    show();
                }
                return true;
            }
        });
        setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        setAdapter(countriesAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showStringInSpinner(countriesAdapter.getItem(which));
            }
        });
    }

    private void showStringInSpinner(String stringToBeShown) {
        String[] currentList = new String[]{stringToBeShown};
        pickedCountry = CountryCode.getCountryCodeFromString(stringToBeShown);
        CountriesListAdapter countriesListAdapter = new CountriesListAdapter(context, currentList);
        countriesAdapter.setCountryNameVisible(false);
        spinner.setAdapter(countriesListAdapter);
        spinner.setSelection(0);
    }

    public int getCurrentCountryCode() {
        return pickedCountry.getCode();
    }
}
