package com.bitrush.meetus.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bitrush.meetus.R;
import com.bitrush.meetus.database.ContactSqlTableHelper;

/**
 * @author ge3k on 1/11/14.
 */
public class ClientCursorAdapter extends ResourceCursorAdapter {

    private LayoutInflater mLayoutInflator;

    public ClientCursorAdapter(Context context, int layout, Cursor c, int flags, LayoutInflater layoutInflater) {
        super(context, layout, c, flags);
        this.mLayoutInflator = layoutInflater;
    }

    public static class ContactItemViewHolder {
        int nameIndex;
        int phoneIndex;
        TextView contactName;
        TextView contactNumber;
    }

    //Different way of viewholder used
    // read https://stackoverflow.com/questions/4567969/viewholder-pattern-correctly-implemented-in-custom-cursoradapter
    //https://stackoverflow.com/questions/12223293/cursoradapter-bindview-optimization



    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ContactItemViewHolder holder  =   (ContactItemViewHolder)    view.getTag();
        holder.contactName.setText(cursor.getString(holder.nameIndex));
        holder.contactNumber.setText(cursor.getString(holder.phoneIndex));
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rowView = mLayoutInflator.inflate(
                R.layout.contact_row, parent, false);
        ContactItemViewHolder holder = new ContactItemViewHolder();
        holder.contactName = (TextView) rowView.findViewById(R.id.contact_name);
        holder.contactNumber = (TextView) rowView.findViewById(R.id.contact_number);
        holder.nameIndex   =   cursor.getColumnIndexOrThrow(ContactSqlTableHelper.USER_NAME);
        holder.phoneIndex = cursor.getColumnIndexOrThrow(ContactSqlTableHelper.PHONE_NUM);
        rowView.setTag(holder);
        return rowView;
    }
}
