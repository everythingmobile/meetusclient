package com.bitrush.meetus.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.bitrush.meetus.R;

/**
 * Created by vivek on 9/14/14.
 */
public class SharedPreferencesUtil {

    public static void write(Activity activity, String key, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String read(Activity activity, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        return sharedPref.getString(key, "");
    }
}
