package com.bitrush.meetus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.config.UserMeetUpRelationConfigBuilder;
import com.bitrush.meetus.fragments.AddMeetUpMapFragment;
import com.bitrush.meetus.fragments.PlacesAutoCompleteFragment;
import com.bitrush.meetus.model.*;
import com.bitrush.meetus.util.StaticFunctions;

public class AddMeetUpActivity extends ActionBarActivity {

    public Fragment currentFragment;
    private AddMeetUpMapFragment mapFragment;
    private StartMeetUpMessage currentMeetUp;
    private AddMeetUpMapFragment addMeetUpMapFragment;
    private EditText searchBox;
    private static enum AddMeetUpFragment {
        MAP_ADD, AUTO_C
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentMeetUp = new StartMeetUpMessage();
        currentMeetUp.setAdmin(StaticFunctions.getCurrentUserInfo(this));
        setContentView(R.layout.activity_add_meet_up);
        setListeners();
        getSupportActionBar().hide();
        setTitle("");
        displayFragment(AddMeetUpFragment.MAP_ADD, null);
    }

    private void setListeners() {
        searchBox = (EditText) findViewById(R.id.autoComplete);
        searchBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayFragment(AddMeetUpFragment.AUTO_C, null);
            }
        });
    }

    public void displayFragment(AddMeetUpFragment addMeetUpFragment, Object parameter)
    {
        Fragment fragment = null;
        switch (addMeetUpFragment) {
            case MAP_ADD:
                mapFragment = AddMeetUpMapFragment.newInstance(this, (Location)parameter);
                addMeetUpMapFragment = mapFragment;
                fragment = mapFragment;
                break;
            case AUTO_C:
                fragment = PlacesAutoCompleteFragment.newInstance(this);
                break;
            default:
                break;
        }

        if(fragment != null)
        {
            fragment.setRetainInstance(true);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.placeholderFragment, fragment);
            fragmentTransaction.addToBackStack(null).commit();
            currentFragment = fragment;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void hideSearchBox() {
        View view = findViewById(R.id.completeSearchBox);
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }


    public void showSearchBox() {
        View view = findViewById(R.id.completeSearchBox);
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public void setCurrentMeetUpLocationAndMarkItOnMap(Location location) {
        currentMeetUp.setMeetUpLocation(location);
        searchBox.setText(location.getLocationDescription() + "," + location.getLocationAddress());
        searchBox.setLines(1);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);
        this.getSupportFragmentManager().popBackStack();
        addMeetUpMapFragment.markMeetUpLocation(location);
    }

    public void setCurrentMeetUpLocation(Location location) {
        currentMeetUp.setMeetUpLocation(location);
        searchBox.setText(location.getLocationDescription() + "," + location.getLocationAddress());
        searchBox.setLines(1);
    }

    public void meetUpLocationFinalised() {
        /*displayFragment(AddMeetUpFragment.MEET_DETAILS, currentMeetUp);*/
        Intent intent = new Intent(this, MeetupInfoActivity.class);
        UserMeetUpRelationConfig config = new UserMeetUpRelationConfigBuilder()
                .setIsGoing(true)
                .setIsUserAdmin(true)
                .createMeetupDetailsFragmentConfig();
        intent.putExtra(MeetupInfoActivity.INTENT_PARAM_MEETUP_CONFIG, config);
        intent.putExtra(MeetupInfoActivity.INTENT_PARAM_START_UP_MESSAGE, currentMeetUp);
        finish();
        startActivity(intent);
    }
}
