package com.bitrush.meetus.util;

import android.os.AsyncTask;

import com.bitrush.meetus.DummySpinnerActivity;
import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.fragments.*;
import com.bitrush.meetus.handlers.*;
import com.bitrush.meetus.model.*;
import com.bitrush.meetus.serializer.StartMeetUpMessageDateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * Created by vivek on 9/13/14.
 */
public class Communicator {

    public static final String HOSTNAME = "ec2-52-74-140-186.ap-southeast-1.compute.amazonaws.com";
    public static  String mServerAddress = HOSTNAME +":8080";
    private static final String PLACE_DETAILS_API_BASE = "maps.googleapis.com/maps/api/place/details/json";
    private static final MediaType JSON  = MediaType.parse("application/json; charset=utf-8");

    public static void login(UserInfo userInfo, LoginFragment loginFragment) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);
        outboundRequest.setPath("/MeetusServer/verifyRegistration");

        Gson gson = new Gson();
        String jsonString = gson.toJson(userInfo);

        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        LoginTaskHandler loginTaskHandler = new LoginTaskHandler();
        loginTaskHandler.setLoginFragment(loginFragment);
        new RestRequestTask(loginTaskHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void userAdd(UserInfo userInfo, RegisterFragment registerFragment) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);
        outboundRequest.setPath("/MeetusServer/useradd");

        Gson gson = new Gson();
        String jsonString = gson.toJson(userInfo);

        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        UserAddTaskHandler userAddTaskHandler = new UserAddTaskHandler();
        userAddTaskHandler.setRegisterFragment(registerFragment);
        new RestRequestTask(userAddTaskHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void verify(Verification verification, VerificationFragment verificationFragment) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);
        outboundRequest.setPath("/MeetusServer/verifyUser");

        Gson gson = new Gson();
        String jsonString = gson.toJson(verification);

        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        VerificationTaskHandler verificationTaskHandler = new VerificationTaskHandler();
        verificationTaskHandler.setVerificationFragment(verificationFragment);
        new RestRequestTask(verificationTaskHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void verifyContacts(UserInfo myUser, List<UserInfo> contacts, LoginRegisterActivity parentActivity) {
        UserAndContacts userAndContacts = new UserAndContacts();
        userAndContacts.setUserContacts(contacts);
        userAndContacts.setUserInfo(myUser);

        Gson gson = new Gson();
        String jsonString = gson.toJson(userAndContacts);

        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);

        outboundRequest.setPath("/MeetusServer/verifyFriends");
        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        UserInfoAcceptHandler userInfoAcceptHandler = new UserInfoAcceptHandler();
        userInfoAcceptHandler.setParentActivity(parentActivity);
        new RestRequestTask(userInfoAcceptHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void getFeed(UserInfo userInfo, FeedDisplayFragment feedDisplayFragment) {
        OutboundRequest outboundRequest = createPathLessRequest(userInfo);
        outboundRequest.setPath("/MeetusServer/getUserFeed");

        FeedHandler feedHandler = new FeedHandler();
        feedHandler.setParentFragment(feedDisplayFragment);
        new RestRequestTask(feedHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void getCurrentMeetups(UserInfo userInfo, CurrentMeetupsFragment fragment) {
        OutboundRequest outboundRequest = createPathLessRequest(userInfo);
        outboundRequest.setPath("/MeetusServer/getUserMeetUps");

        CurrentMeetupsHandler feedHandler = new CurrentMeetupsHandler();
        feedHandler.setParentFragment(fragment);
        new RestRequestTask(feedHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }


    public  static OutboundRequest createPathLessRequest(UserInfo userInfo) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);
        outboundRequest.getParameters().put("countryCode", Integer.toString(userInfo.getPhoneInfo().getCountryCode()));
        outboundRequest.getParameters().put("phoneNumber", Long.toString(userInfo.getPhoneInfo().getPhoneNumber()));
        outboundRequest.getParameters().put("pageNum", "1");
        outboundRequest.setRequestMethod("GET");
        return outboundRequest;

    }

    public static void getLocationFromGivenAddress(String placeId, PlacesAutoCompleteFragment placesAutoCompleteFragment) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(PLACE_DETAILS_API_BASE);
        outboundRequest.getParameters().put("placeid", URLEncoder.encode(placeId));
        outboundRequest.getParameters().put("key", PlacesUtil.API_KEY);
        outboundRequest.setRequestMethod("GET");
        outboundRequest.setProtocol(OutboundRequest.Protocol.HTTPS);

        GeoEncodingHandler geoEncodingHandler = new GeoEncodingHandler(placesAutoCompleteFragment);
        new RestRequestTask(geoEncodingHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void addMeetUp(StartMeetUpMessage startMeetUpMessage, MeetUpDetailsFragment meetUpDetailsFragment) {
        GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Date.class, new StartMeetUpMessageDateSerializer());
        Gson gson = gsonBuilder.create();
        String jsonString = gson.toJson(startMeetUpMessage);

        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);

        outboundRequest.setPath("/MeetusServer/addMeetUp");
        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        AddMeetUpHandler addMeetUpHandler = new AddMeetUpHandler(meetUpDetailsFragment);
        new RestRequestTask(addMeetUpHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void updateMeetUp(MeetUpEntityChanges changes, MeetUpDetailsFragment thisF) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(changes);

        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);

        outboundRequest.setPath("/MeetusServer/updateMeetUp");
        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        UpdateMeetUpHandler updateMeetUpHandler = new UpdateMeetUpHandler(thisF);
        new RestRequestTask(updateMeetUpHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void addComment(UserComment userComment, CommentsFragment commentsFragment) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(userComment);

        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);

        outboundRequest.setPath("/MeetusServer/addUserComment");
        RequestBody requestBody = RequestBody.create(JSON, jsonString);
        outboundRequest.setRequestMethod("POST");
        outboundRequest.setRequestBody(requestBody);

        AddCommentHandler addCommentHandler = new AddCommentHandler();
        addCommentHandler.setCommentsFragment(commentsFragment);
        new RestRequestTask(addCommentHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void getComments(String meetUpId, PhoneInfo phoneInfo, CommentsFragment commentsFragment) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);
        outboundRequest.setPath("/MeetusServer/getMeetUpComments");
        outboundRequest.getParameters().put("meetUpId", meetUpId);
        outboundRequest.getParameters().put("countryCode", Integer.toString(phoneInfo.getCountryCode()));
        outboundRequest.getParameters().put("phoneNumber", Long.toString(phoneInfo.getPhoneNumber()));
        outboundRequest.setRequestMethod("GET");
        outboundRequest.setProtocol(OutboundRequest.Protocol.HTTP);

        GetCommentsHandler getCommentsHandler = new GetCommentsHandler(commentsFragment);
        new RestRequestTask(getCommentsHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    public static void getMeetUpDetails(String meetUpId, PhoneInfo phoneInfo, DummySpinnerActivity dummySpinnerActivity) {
        OutboundRequest outboundRequest = getOutboundRequestForMeetUpDetails(meetUpId, phoneInfo);

        GetMeetUpDetailsHandler getMeetUpDetailsHandler = new GetMeetUpDetailsHandler(dummySpinnerActivity, meetUpId);
        new RestRequestTask(getMeetUpDetailsHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }

    private static OutboundRequest getOutboundRequestForMeetUpDetails(String meetUpId, PhoneInfo phoneInfo) {
        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost(mServerAddress);
        outboundRequest.setPath("/MeetusServer/getMeetUpDetails");
        outboundRequest.getParameters().put("meetUpId", meetUpId);
        outboundRequest.getParameters().put("countryCode", Integer.toString(phoneInfo.getCountryCode()));
        outboundRequest.getParameters().put("phoneNumber", Long.toString(phoneInfo.getPhoneNumber()));
        outboundRequest.setRequestMethod("GET");
        outboundRequest.setProtocol(OutboundRequest.Protocol.HTTP);
        return outboundRequest;
    }

    public static void getMeetUpDetailsAndComments(String meetUpId, PhoneInfo phoneInfo, DummySpinnerActivity dummySpinnerActivity) {
        OutboundRequest outboundRequest = getOutboundRequestForMeetUpDetails(meetUpId, phoneInfo);

        GetMeetUpDetailsHandler getMeetUpDetailsHandler = new GetMeetUpDetailsHandler(dummySpinnerActivity, meetUpId);
        getMeetUpDetailsHandler.setCommentsTrue();
        new RestRequestTask(getMeetUpDetailsHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);
    }
}
