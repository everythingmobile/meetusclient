package com.bitrush.meetus.model;

import java.io.Serializable;

/**
 * @author ge3k on 5/7/14.
 */
public class MeetusInitMessage implements Serializable {

    private String MessageHead;
    private String MeetUpId;
    private String UserToken;
    private int UserCountryCode;
    private Long UserPhoneNumber;

    public int getUserCountryCode() {
        return UserCountryCode;
    }

    public void setUserCountryCode(int userCountryCode) {
        UserCountryCode = userCountryCode;
    }

    public String getMessageHead() {
        return MessageHead;
    }

    public void setMessageHead(String messageHead) {
        MessageHead = messageHead;
    }

    public String getMeetUpId() {
        return MeetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        MeetUpId = meetUpId;
    }

    public String getUserToken() {
        return UserToken;
    }

    public void setUserToken(String userToken) {
        UserToken = userToken;
    }

    public Long getUserPhoneNumber() {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber(Long userPhoneNumber) {
        UserPhoneNumber = userPhoneNumber;
    }
}
