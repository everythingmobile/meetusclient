package com.bitrush.meetus.util;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.bitrush.meetus.model.OutboundRequest;
import com.squareup.mimecraft.FormEncoding;
import com.squareup.mimecraft.Multipart;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.OkUrlFactory;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by vivek on 2/25/14.
 */
public class RestRequestTask extends AsyncTask<OutboundRequest, Integer, String> {
    private HttpURLConnection httpURLConnection;
    private String TAG = "com.pricecheck.util.RestRequestTask";
    private Handler foreignHandler;
    private Integer index;

    public RestRequestTask(Handler foreignHandler, int index) {
        this.foreignHandler = foreignHandler;
        this.index = index;
    }

    public RestRequestTask(Handler handler) {
        foreignHandler = handler;
    }

    @Override
    protected String doInBackground(OutboundRequest... params) {
        String response = null;
        if(params.length > 1) {
            return null;
        }
        for(OutboundRequest outboundRequest : params) {
            try {
                OkHttpClient client = new OkHttpClient();
                RequestBody requestBody = outboundRequest.getRequestBody();
                if (requestBody != null) {
                    return doPostOnRequestBody(outboundRequest,client);
                }
                httpURLConnection = new OkUrlFactory(client).open(new URL(outboundRequest.build()));
                FormEncoding formEncoding = outboundRequest.getFormEncoding();
                String requestMethod = outboundRequest.getRequestMethod();
                httpURLConnection.setRequestMethod(requestMethod);
                if(!requestMethod.equals("GET")) {
                    for (Map.Entry<String, String> entry : formEncoding.getHeaders().entrySet()) {
                        httpURLConnection.addRequestProperty(entry.getKey(), entry.getValue());
                    }
                    formEncoding.writeBodyTo(httpURLConnection.getOutputStream());
                }

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                response = readStream(in);
            } catch (MalformedURLException e) {
                Log.e(TAG, "MalformedURL",e);
            } catch (IOException e) {
                Log.e(TAG, "IOException",e);
            } finally {
                if(httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
        }
        return response;
    }

    private String doPostOnRequestBody(OutboundRequest outboundRequest, OkHttpClient client) throws IOException {
        URL builtUrl = new URL(outboundRequest.build());
        Request request = new Request.Builder()
                .url(builtUrl)
                .post(outboundRequest.getRequestBody())
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder data = new StringBuilder("");
        try  {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                data.append(line);
            }
        } catch (IOException e)  {
            Log.e(TAG, "IOException");
        } finally {
            if(reader != null)  {
                try  {
                    reader.close();
                } catch (IOException e)  {
                    Log.e(TAG, "IOException");
                }
            }
        }
        return data.toString();
    }

    @Override
    protected void onPostExecute(String products) {
        Message message = new Message();
        message.obj = products;
        if(index != null) {
            message.arg1 = index;
        }
        foreignHandler.sendMessage(message);
    }
}
