package com.bitrush.meetus.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.bitrush.meetus.R;
import com.bitrush.meetus.asynctasks.GooglePlacesReadTask;
import com.bitrush.meetus.fragments.AddMeetUpMapFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by vivek on 4/4/15.
 */
public class NearbyPlacesService {
    private static NearbyPlacesService nearbyPlacesService;
    private GoogleMap googleMap;
    private Context context;
    private static final String PROXIMITY_RADIUS = "150";
    private AsyncTask asyncTask;
    private AddMeetUpMapFragment addMeetUpMapFragment;

    public static NearbyPlacesService getInstance(GoogleMap googleMap, Context context, AddMeetUpMapFragment addMeetUpMapFragment) {
        if (nearbyPlacesService == null) {
            nearbyPlacesService = new NearbyPlacesService();
        }
        nearbyPlacesService.googleMap = googleMap;
        nearbyPlacesService.context = context;
        nearbyPlacesService.addMeetUpMapFragment = addMeetUpMapFragment;
        return nearbyPlacesService;
    }

    public void stopRequests() {
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
    }

    public void markNearbyPlaces(LatLng latLng) {
        StringBuilder googlePlacesUrl = new StringBuilder(context.getResources().getString(R.string.nearbyPlacesApiUrl));
        googlePlacesUrl.append("location=").append(latLng.latitude).append(",").append(latLng.longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + "AIzaSyDrDYbf-o203niPeY9_vLXPr2BBww-5RMQ");

        Log.d("myapp", Log.getStackTraceString(new Exception()));
        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask(addMeetUpMapFragment);
        Object[] toPass = new Object[2];
        toPass[0] = googleMap;
        toPass[1] = googlePlacesUrl.toString();
        asyncTask = googlePlacesReadTask.execute(toPass);
    }
}
