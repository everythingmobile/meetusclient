package com.bitrush.meetus.model;

/**
 * Created by vivek on 12/28/14.
 */
public class Place {
    private String description;
    private String placeID;

    public Place() {
    }

    public Place(String description, String placeID) {
        this.description = description;
        this.placeID = placeID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }
}
