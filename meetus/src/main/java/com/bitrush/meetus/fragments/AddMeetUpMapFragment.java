package com.bitrush.meetus.fragments;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import at.markushi.ui.CircleButton;
import com.bitrush.meetus.AddMeetUpActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.location.AddMeetUpLocationHelper;
import com.bitrush.meetus.services.NearbyPlacesService;
import com.bitrush.meetus.util.MeetusLocationUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

/**
 * Created by vivek on 10/25/14.
 */
public class AddMeetUpMapFragment extends Fragment{
    private static GoogleMap mMap;
    private com.bitrush.meetus.model.Location meetUpLocation;
    private static AddMeetUpActivity parentActivity;
    private static Location currentLocationStorage;
    private SupportMapFragment supportMapFragment;
    private AddMeetUpLocationHelper locationHelper;
    private CircleButton zoomButton;
    private static LatLng newPositionOfMarker;
    private CircleButton doneButton;
    private View currentView;
    private static Marker draggableMarker;
    private HashMap<Marker, com.bitrush.meetus.model.Location> markers;

    public static AddMeetUpMapFragment newInstance(AddMeetUpActivity parentActivity, com.bitrush.meetus.model.Location location) {
        AddMeetUpMapFragment.parentActivity = parentActivity;
        AddMeetUpMapFragment instance = new AddMeetUpMapFragment();
        instance.meetUpLocation = location;
        AddMeetUpMapFragment.newPositionOfMarker = null;
        AddMeetUpMapFragment.currentLocationStorage = null;
        AddMeetUpMapFragment.mMap = null;
        AddMeetUpMapFragment.draggableMarker = null;
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        currentView = inflater.inflate(R.layout.add_meet_up_map_fragment, container, false);
        setListeners();
        locationHelper = new AddMeetUpLocationHelper(this);
        return currentView;
    }

    private void setListeners() {
        zoomButton = (CircleButton) currentView.findViewById(R.id.zoomToCurrentLocationButton);
        doneButton = (CircleButton) currentView.findViewById(R.id.doneButton);
        zoomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomToCurrentLocation();
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meetUpLocationFinalised();
            }
        });
    }

    private void meetUpLocationFinalised() {
        parentActivity.meetUpLocationFinalised();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.mapFragment);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.mapFragment, supportMapFragment).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.showSearchBox();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = supportMapFragment.getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    public Activity getParentActivity() {
        return parentActivity;
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    private void updateZoom(LatLng myLatLng) {
        LatLngBounds bounds = MeetusLocationUtil.calculateBoundsWithCenter(myLatLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 10));
    }

    public void markMeetUpLocation(final com.bitrush.meetus.model.Location location) {
        final com.bitrush.meetus.model.Location finalLocation = location;
        meetUpLocation = location;
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.clear();
                MeetusLocationUtil.zoomToLocation(finalLocation, mMap);
                newPositionOfMarker = new LatLng(Double.parseDouble(location.getDestinationLatitude()), Double.parseDouble(location.getDestinationLongitude()));
                addMarkerToMap(null);
            }
        });
    }

    public void startByZoomingToCurrentLocation() {
        if(meetUpLocation != null) {
            return;
        }
        zoomToCurrentLocation();
    }

    private void zoomToCurrentLocation() {
        final Location currentLocation = locationHelper.getLocation();
        final AddMeetUpMapFragment thisF = this;
        if(currentLocation != null) {
            currentLocationStorage = currentLocation;
            newPositionOfMarker = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            NearbyPlacesService.getInstance(mMap, parentActivity, thisF).markNearbyPlaces(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            addMarkerToMap(currentLocation);
            setMapListeners(thisF);
            updateZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
        }
    }

    private void setMapListeners(final AddMeetUpMapFragment thisF) {
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                NearbyPlacesService.getInstance(mMap, parentActivity, thisF).stopRequests();
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                LatLng newPosition = marker.getPosition();
                mMap.animateCamera(CameraUpdateFactory.newLatLng(newPosition));
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                NearbyPlacesService.getInstance(mMap, parentActivity, thisF).markNearbyPlaces(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                newPositionOfMarker = marker.getPosition();
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (!markers.containsKey(marker)) {
                    return true;
                }
                meetUpLocation = markers.get(marker);
                draggableMarker.setPosition(new LatLng(Double.parseDouble(meetUpLocation.getDestinationLatitude()), Double.parseDouble(meetUpLocation.getDestinationLongitude())));
                draggableMarker.setTitle(meetUpLocation.getLocationDescription() + "," + meetUpLocation.getLocationAddress());
                draggableMarker.showInfoWindow();
                parentActivity.setCurrentMeetUpLocation(meetUpLocation);
                return true;
            }
        });
    }

    public static void addMarkerToMap(Location currentLocation) {
        LatLng location = null;
        if (currentLocation == null) {
            location = newPositionOfMarker;
        } else if (newPositionOfMarker == null) {
            location = new LatLng(currentLocationStorage.getLatitude(), currentLocation.getLongitude());
        } else {
            location = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        }
        draggableMarker = mMap.addMarker(new MarkerOptions().position(location).draggable(true));
    }


    public void setMarkers(HashMap<Marker, com.bitrush.meetus.model.Location> markers) {
        this.markers = markers;
    }
}
