package com.bitrush.meetus.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitrush.meetus.MeetUpsActivity;
import com.bitrush.meetus.MeetupInfoActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.MeetUpRecycleAdapter;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.config.UserMeetUpRelationConfigBuilder;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.SharedPreferencesUtil;
import com.bitrush.meetus.util.StaticFunctions;
import com.bitrush.meetus.util.PullToRefreshHelper;
import com.bitrush.meetus.util.StaticFunctions;

import java.util.List;


public class FeedDisplayFragment extends AbstractFeedFragment {

    @Override
    public void getMeetUps() {
        UserInfo userInfo = StaticFunctions.getCurrentUserInfo(parentActivity);
        Communicator.getFeed(userInfo, this);
    }

    public static FeedDisplayFragment newInstance(MeetUpsActivity meetUpsActivity) {
        FeedDisplayFragment.parentActivity = meetUpsActivity;
        FeedDisplayFragment fragment =  new FeedDisplayFragment();
        fragment.setMeetupsType("getUserFeed");
        return fragment;
    }


    public void setMeetUps(List<MeetUpEntity> meetUpEntities) {
        meetUpsAdapter.setMeetUps(meetUpEntities);
        meetUpsAdapter.notifyDataSetChanged();
    }

    public void popUp(Intent intent) {
        startActivity(intent);
    }

    public void showMeetUpInfo(StartMeetUpMessage startMeetUpMessage, String meetUpId) {
        UserInfo userInfo = StaticFunctions.getCurrentUserInfo(parentActivity);

        UserMeetUpRelationConfig config = new UserMeetUpRelationConfigBuilder()
                .setIsUserAdmin(userInfo == startMeetUpMessage.getAdmin()).
                        setIsGoing(isUserParticipating(startMeetUpMessage)).createMeetupDetailsFragmentConfig();

        Intent intent = new Intent(parentActivity, MeetupInfoActivity.class);
        intent.putExtra(MeetupInfoActivity.INTENT_PARAM_MEETUP_CONFIG, config);
        intent.putExtra(MeetupInfoActivity.INTENT_PARAM_START_UP_MESSAGE, startMeetUpMessage);
        intent.putExtra(MeetupInfoActivity.INTENT_PARAM_MEETUP_ID, meetUpId);
        startActivity(intent);
    }

    private boolean isUserParticipating(StartMeetUpMessage startMeetUpMessage) {
        UserInfo userInfo = StaticFunctions.getCurrentUserInfo(parentActivity);
        return startMeetUpMessage.getInvitees().indexOf(userInfo) != -1;

    }
}
