package com.bitrush.meetus.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * Created by vivek on 9/21/14.
 */
public class MapUser {
    private List<LatLng> userLocationList;
    private String userId;
    private Marker lastMarker;
    private Integer userColor;

    public MapUser(String userId, List<LatLng> userLocationList) {
        this.userId = userId;
        this.userLocationList = userLocationList;
    }

    public List<LatLng> getUserLocationList() {
        return userLocationList;
    }

    public void setUserLocationList(List<LatLng> userLocationList) {
        this.userLocationList = userLocationList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Marker getLastMarker() {
        return lastMarker;
    }

    public void setLastMarker(Marker lastMarker) {
        this.lastMarker = lastMarker;
    }

    public Integer getUserColor() {
        return userColor;
    }

    public void setUserColor(Integer userColor) {
        this.userColor = userColor;
    }
}
