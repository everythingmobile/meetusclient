package com.bitrush.meetus.asynctasks;

import android.os.AsyncTask;
import android.util.Log;
import com.bitrush.meetus.fragments.AddMeetUpMapFragment;
import com.bitrush.meetus.util.SimpleHttp;
import com.google.android.gms.maps.GoogleMap;

/**
 * Created by vivek on 4/4/15.
 */
public class GooglePlacesReadTask extends AsyncTask<Object, Integer, String> {
    private final AddMeetUpMapFragment addMeetUpMapFragment;
    String googlePlacesData = null;
    GoogleMap googleMap;

    public GooglePlacesReadTask(AddMeetUpMapFragment addMeetUpMapFragment) {
        this.addMeetUpMapFragment = addMeetUpMapFragment;
    }

    @Override
    protected String doInBackground(Object... inputObj) {
        try {
            googleMap = (GoogleMap) inputObj[0];
            String googlePlacesUrl = (String) inputObj[1];
            SimpleHttp simpleHttp = new SimpleHttp();
            googlePlacesData = simpleHttp.read(googlePlacesUrl);
        } catch (Exception e) {
            Log.d("Google Place Read Task", e.toString());
        }
        return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String result) {
        PlacesDisplayTask placesDisplayTask = new PlacesDisplayTask(addMeetUpMapFragment);
        Object[] toPass = new Object[2];
        toPass[0] = googleMap;
        toPass[1] = result;
        placesDisplayTask.execute(toPass);
    }
}