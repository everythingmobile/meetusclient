package com.bitrush.meetus.model;

import java.io.Serializable;

/**
 * Created by vivek on 12/13/14.
 */
public class PhoneInfo implements Serializable, Comparable{
    private int countryCode;
    private long phoneNumber;

    public PhoneInfo(int countryCode, long phoneNumber) {
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
    }

    public PhoneInfo() {
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PhoneInfo{");
        sb.append("countryCode=").append(countryCode);
        sb.append(", phoneNumber=").append(phoneNumber);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        PhoneInfo phoneInfo = (PhoneInfo) o;
        return this.toString().compareTo(phoneInfo.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        PhoneInfo phoneInfo = (PhoneInfo) obj;
        return this.countryCode == phoneInfo.getCountryCode() && this.phoneNumber == phoneInfo.getPhoneNumber();
    }

    @Override
    public int hashCode() {
        return countryCode + ((Long)phoneNumber).hashCode();
    }
}
