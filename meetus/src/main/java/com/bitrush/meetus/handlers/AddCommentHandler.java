package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.bitrush.meetus.fragments.CommentsFragment;

/**
 * Created by vivek on 4/19/15.
 */
public class AddCommentHandler extends Handler {
    private CommentsFragment commentsFragment;

    public void setCommentsFragment(CommentsFragment commentsFragment) {
        this.commentsFragment = commentsFragment;
    }

    @Override
    public void handleMessage(Message msg) {
        String response = (String) msg.obj;
        if (response == null || response.equals("")) {
            return;
        }
        Log.i("APPTAG","Comment added");
    }
}
