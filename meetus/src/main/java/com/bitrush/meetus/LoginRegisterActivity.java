package com.bitrush.meetus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.bitrush.meetus.fragments.AppStartYesNoFragment;
import com.bitrush.meetus.fragments.LoginFragment;
import com.bitrush.meetus.fragments.RegisterFragment;
import com.bitrush.meetus.fragments.SyncFragment;
import com.bitrush.meetus.fragments.VerificationFragment;
import com.bitrush.meetus.util.PlayServiceHelper;
import com.parse.Parse;
import com.parse.ParseInstallation;

public class LoginRegisterActivity extends FragmentActivity {

    public static enum LoginRegisterFragments {
        YESNO, REGISTER, LOGIN, VERIFY, SYNC
    }


    public LoginRegisterFragments currentFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_register);
        if (PlayServiceHelper.checkPlayServices(this)) {
            displayFragment(LoginRegisterFragments.YESNO);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!PlayServiceHelper.checkPlayServices(this)) {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void displayFragment(LoginRegisterFragments loginRegisterFragment)
    {
        Fragment fragment = null;
        switch (loginRegisterFragment) {
            case YESNO:
                fragment = AppStartYesNoFragment.newInstance(this);
                break;
            case LOGIN:
                fragment = LoginFragment.newInstance(this);
                break;
            case REGISTER:
                fragment = RegisterFragment.newInstance(this);
                break;
            case VERIFY:
                fragment = VerificationFragment.newInstance(this);
                break;
            case SYNC:
                fragment = SyncFragment.newInstance(this);
                break;
        }

        if(fragment != null)
        {
            fragment.setRetainInstance(true);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.loginRegisterFragment, fragment);
            fragmentTransaction.addToBackStack("fragTag"); //TODO use proper tag
            fragmentTransaction.commit();
            currentFragment = loginRegisterFragment;
        }
    }

    public void startVerification() {
        displayFragment(LoginRegisterFragments.VERIFY);
    }

    public void verificationDone() {
        Intent intent = new Intent(getApplicationContext(), MeetUpsActivity.class);
        startActivity(intent);
    }

    public void yesNoButtonPressed(boolean yesButtonPressed) {
        displayFragment(yesButtonPressed ? LoginRegisterFragments.LOGIN : LoginRegisterFragments.REGISTER);
    }

}
