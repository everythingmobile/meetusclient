package com.bitrush.meetus.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vivek on 8/2/14.
 */
public class StartMeetUpMessage implements Parcelable {
    private UserInfo admin;
    private Location meetUpLocation;
    private Date startUpTime;
    private List<UserInfo> invitees;

    public StartMeetUpMessage() {
    }

    public Date getStartUpTime() {
        return startUpTime;
    }

    public void setStartUpTime(Date startUpTime) {
        this.startUpTime = startUpTime;
    }

    public UserInfo getAdmin() {
        return admin;
    }

    public void setAdmin(UserInfo admin) {
        this.admin = admin;
    }

    public Location getMeetUpLocation() {
        return meetUpLocation;
    }

    public void setMeetUpLocation(Location meetUpLocation) {
        this.meetUpLocation = meetUpLocation;
    }

    public List<UserInfo> getInvitees() {
        return invitees;
    }

    public void setInvitees(List<UserInfo> invitees) {
        this.invitees = invitees;
    }

    public static final Parcelable.Creator<StartMeetUpMessage> CREATOR = new Parcelable.Creator<StartMeetUpMessage>() {
        @Override
        public StartMeetUpMessage createFromParcel(Parcel source) {
            return new StartMeetUpMessage(source);
        }

        @Override
        public StartMeetUpMessage[] newArray(int size) {
            return new StartMeetUpMessage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(admin);
        dest.writeValue(meetUpLocation);
        dest.writeValue(startUpTime);
        dest.writeList(invitees);
    }

    private StartMeetUpMessage(Parcel in) {
        this.admin = (UserInfo) in.readValue(UserInfo.class.getClassLoader());
        this.meetUpLocation = (Location) in.readValue(Location.class.getClassLoader());
        this.startUpTime = (Date) in.readValue(Date.class.getClassLoader());
        this.invitees = in.readArrayList(UserInfo.class.getClassLoader());
    }
}
