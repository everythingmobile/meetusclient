package com.bitrush.meetus.handlers;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.MeetusNavigationActivity;
import com.bitrush.meetus.model.MeetusUserDetail;

public class UserLocationWebSocketHandler extends Handler {
    private MeetusNavigationActivity parentActivity;

    @Override
    public void handleMessage(Message inputMessage) {
        if (inputMessage.what == MeetusConstants.MESSAGE_KEY_LOCATION) {
            Log.i(MeetusConstants.APPTAG, "Got a message from websocket about the user location change");
            Bundle dataBundle = inputMessage.getData();
            MeetusUserDetail regularMessage = (MeetusUserDetail) dataBundle.
                    getSerializable(MeetusConstants.LOCATION_CHANGED_SERIALIZABLE_KEY);
            Log.i(MeetusConstants.APPTAG, "User detail changed is : " + regularMessage);
            parentActivity.locationChangedForFriend(regularMessage);
        }
    }

    public void setParentActivity(MeetusNavigationActivity parentActivity) {
        this.parentActivity = parentActivity;
    }

    public MeetusNavigationActivity getParentActivity() {
        return parentActivity;
    }
}
