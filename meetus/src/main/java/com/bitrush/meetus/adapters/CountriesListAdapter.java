package com.bitrush.meetus.adapters;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.model.CountryCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CountriesListAdapter extends ArrayAdapter<String> {
	private final Context context;
    private final LoginRegisterActivity parentActivity;
    private LayoutInflater inflater;
    private List<CountryCode> countryCodes = new ArrayList<>();
    private boolean countryNameVisible = true;

    static class ViewHolder {
        public TextView countryName;
        public TextView countryPhoneCode;
        public ImageView flagImage;
    }

    public CountriesListAdapter(Context context, String[] values) {
		super(context, R.layout.country_list_item, values);
		this.context = context;
        this.parentActivity = (LoginRegisterActivity) context;
        for (String value : values) {
            countryCodes.add(CountryCode.getCountryCodeFromString(value));
        }
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

    private int getCurrentIsd() {
        TelephonyManager manager = (TelephonyManager) parentActivity.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null) {
            String countryID= manager.getSimCountryIso().toUpperCase();
            String[] countryCodeMap=parentActivity.getResources().getStringArray(R.array.CountryCodes);
            for (String aCountryCodeMap : countryCodeMap) {
                String[] parts = aCountryCodeMap.split(",");
                if (parts[1].trim().equals(countryID.trim())) {
                    return Integer.parseInt(parts[0]);
                }
            }
        }
        return 0;
    }

    public int getCurrentIsdPosition() {
        int currentCountryCode = getCurrentIsd();
        if (currentCountryCode == 0) {
            return 0;
        }
        for (int i = 0; i < countryCodes.size(); i++) {
            if (countryCodes.get(i).getCode() == currentCountryCode) {
                return i;
            }
        }
        return 0;
    }

    public void setCountryNameVisible(boolean countryNameVisible) {
        this.countryNameVisible = countryNameVisible;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position,convertView,parent);
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (rowView == null) {
            rowView = inflater.inflate(R.layout.country_list_item, parent, false);
            viewHolder.countryName = (TextView) rowView.findViewById(R.id.txtViewCountryName);
            viewHolder.countryPhoneCode = (TextView) rowView.findViewById(R.id.countryPhoneCode);
            viewHolder.flagImage = (ImageView) rowView.findViewById(R.id.imgViewFlag);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        if (!countryNameVisible) {
            holder.countryName.setText(countryCodes.get(position).getName());
        }
        holder.countryPhoneCode.setText(" +" + Integer.toString(countryCodes.get(position).getCode()));
    	String pngName = getCountryDrawableFromName(countryCodes.get(position).getName());
    	holder.flagImage.setImageResource(context.getResources().getIdentifier("drawable/" + pngName, null, context.getPackageName()));
		return rowView;
	}

    private String getCountryDrawableFromName(String name) {
        String drawableName = name.replace(" ", "_");
        drawableName = drawableName.replace("\'", "");
        return drawableName.toLowerCase();
    }
}