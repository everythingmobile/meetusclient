package com.bitrush.meetus.contentprovider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.bitrush.meetus.database.ContactSqlTableHelper;

import java.util.Arrays;
import java.util.HashSet;

/**
 * @author ge3k on 25/10/14.
 */
public class MeetusContactProvider extends ContentProvider {

    private ContactSqlTableHelper database;
    private static final String AUTHORITY = "com.bitrush.meetus.contentprovider";
    private static final String BASE_PATH = "meetusContact";
    public static final int CONTACT_PHN = 12;
    private static final int CONTACT_NAME = 25;
    private static final int COUNTRY_CODE = 42;

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    @Override
    public boolean onCreate() {
        database = new ContactSqlTableHelper(this.getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        checkColumns(projection);
        queryBuilder.setTables(ContactSqlTableHelper.TABLE_CONTACTS);
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case CONTACT_PHN:
                queryBuilder.appendWhere(ContactSqlTableHelper.PHONE_NUM + "="
                        + uri.getLastPathSegment());
                break;
            case CONTACT_NAME:
                queryBuilder.appendWhere(ContactSqlTableHelper.USER_NAME + "=" +
                uri.getLastPathSegment());
                break;
            case COUNTRY_CODE:
                queryBuilder.appendWhere(ContactSqlTableHelper.COUNTRY_CODE + "=" +
                        uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id;
        switch (uriType) {
            case CONTACT_NAME:
                id = sqlDB.insert(ContactSqlTableHelper.TABLE_CONTACTS, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case CONTACT_PHN:
                rowsDeleted = sqlDB.delete(ContactSqlTableHelper.TABLE_CONTACTS, selection,
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case CONTACT_PHN:
                String phoneNumber = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(ContactSqlTableHelper.TABLE_CONTACTS,
                            values,
                            ContactSqlTableHelper.PHONE_NUM + "=" + phoneNumber,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(ContactSqlTableHelper.TABLE_CONTACTS,
                            values,
                            ContactSqlTableHelper.PHONE_NUM + "=" + phoneNumber
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }


    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, CONTACT_PHN);
        sURIMatcher.addURI(AUTHORITY,BASE_PATH, CONTACT_NAME);
    }

    private void checkColumns(String[] projection) {
        String[] available = { ContactSqlTableHelper.COLUMN_ID,
                ContactSqlTableHelper.USER_NAME, ContactSqlTableHelper.EMAIL_ADD,
                ContactSqlTableHelper.PHONE_NUM };
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }

}
