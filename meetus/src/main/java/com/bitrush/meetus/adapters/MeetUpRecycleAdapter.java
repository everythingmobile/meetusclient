package com.bitrush.meetus.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitrush.meetus.R;
import com.bitrush.meetus.fragments.AbstractFeedFragment;
import com.bitrush.meetus.fragments.FeedDisplayFragment;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.StaticFunctions;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ge3k on 24/1/15.
 */
public class MeetUpRecycleAdapter extends RecyclerView.Adapter<MeetUpRecycleAdapter.ViewHolder> {
    private AbstractFeedFragment parentView;
    private List<MeetUpEntity> meetUps;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextView;
        public ImageView mImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.meetUpDesc);
            mImageView = (ImageView) itemView.findViewById(R.id.staticView);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public void setMeetUps(List<MeetUpEntity> meetUps) {
        this.meetUps = meetUps;
    }

    public MeetUpRecycleAdapter(Context mContext, AbstractFeedFragment parentView) {
        this.mContext = mContext;
        this.parentView = parentView;
        this.meetUps = new ArrayList<>();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MeetUpRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {

        View meetupItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.meetup, parent, false);
        return new ViewHolder(meetupItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String meetUpDesc = StaticFunctions.getDescription(meetUps.get(position));
        Picasso picasso = Picasso.with(mContext);
        picasso.setLoggingEnabled(true);
        picasso.load(StaticFunctions.getUrlForStaticMap(meetUps.get(position))).into(holder.mImageView);

        //java.io.IOException: Illegal character in query at index 126: http://maps.google.com/maps/api/staticmap?center=13.66,78.23&zoom=12&size=265x65&maptype=roadmap&sensor=true&markers=color:red|13.66,78.23&key=AIzaSyBXCr_NyVD66W2pkjUPVP7nNVq7Z2BmbSQ
        if (!meetUpDesc.equals("")) {
            holder.mTextView.setText(meetUpDesc);
        }
        final MeetUpEntity meetUpEntity = meetUps.get(position);
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMeetUpInfoActivity(meetUpEntity);
            }
        });
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMeetUpInfoActivity(meetUpEntity);
            }
        });
    }

    private void setOnClickListeners(View view, final LatLng latLng) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double latitude = latLng.latitude;
                double longitude = latLng.longitude;
                String label = "Meetup point";
                String uriBegin = "geo:" + latitude + "," + longitude;
                String query = latitude + "," + longitude + "(" + label + ")";
                String encodedQuery = Uri.encode(query);
                String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                Uri uri = Uri.parse(uriString);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                parentView.popUp(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (meetUps == null) {
            return 0;
        }
        return meetUps.size();
    }

    private void startMeetUpInfoActivity(MeetUpEntity meetUpEntity) {
        StartMeetUpMessage startMeetUpMessage = new StartMeetUpMessage();
        startMeetUpMessage.setAdmin(new UserInfo(meetUpEntity.getAdminPhoneInfo(), "", ""));
        startMeetUpMessage.setInvitees(meetUpEntity.getParticipants());
        startMeetUpMessage.setMeetUpLocation(meetUpEntity.getMeetUpLocation());
        startMeetUpMessage.setStartUpTime(new Date(meetUpEntity.getStartUpTime()));

        parentView.showMeetUpInfo(startMeetUpMessage, meetUpEntity.getMeetUpId());

    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public List<MeetUpEntity> getMeetUps() {
        return meetUps;
    }

    public AbstractFeedFragment getParentView() {
        return parentView;
    }

    public void setParentView(AbstractFeedFragment parentView) {
        this.parentView = parentView;
    }
}
