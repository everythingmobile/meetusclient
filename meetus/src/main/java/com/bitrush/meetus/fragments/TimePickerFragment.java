package com.bitrush.meetus.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by vivek on 1/3/15.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    public static MeetUpDetailsFragment parentFragment;

    public static TimePickerFragment newInstance(MeetUpDetailsFragment parentFragment) {
        TimePickerFragment.parentFragment = parentFragment;
        return new TimePickerFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hourOfDay = c.get(Calendar.HOUR);
        int minuteOfDay = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hourOfDay, minuteOfDay, false);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String amPm = "AM";
        if(hourOfDay >= 12) {
            amPm = "PM";
            if(hourOfDay > 12) {
                hourOfDay = hourOfDay - 12;
            }
        }
        if (hourOfDay == 0) {
            hourOfDay = 12;
        }
        String time = Integer.toString(hourOfDay) + ":" + Integer.toString(minute) + " " + amPm;
        if(parentFragment != null) {
            parentFragment.setTime(time);
        }
    }
}
