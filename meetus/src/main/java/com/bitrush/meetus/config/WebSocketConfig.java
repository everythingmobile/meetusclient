package com.bitrush.meetus.config;

public final class WebSocketConfig {
    private final String meetupId;
    private final Integer userContryCode;
    private final Long userPhoneNumber;
    private final String token;
    private final String userName;

    public WebSocketConfig(String meetupId, Integer userCountry, Long phoneNumber, String token, String userName) {
        this.meetupId = meetupId;
        this.userContryCode = userCountry;
        this.userPhoneNumber = phoneNumber;
        this.token = token;
        this.userName = userName;
    }

    public String getMeetupId() {
        return meetupId;
    }

    public Integer getUserContryCode() {
        return userContryCode;
    }

    public String getToken() {
        return token;
    }

    public Long getUserPhoneNumber() {
        return userPhoneNumber;
    }
}