package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;

import com.bitrush.meetus.fragments.CurrentMeetupsFragment;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.util.StaticFunctions;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * @author ge3k on 2/11/14.
 */
public class CurrentMeetupsHandler extends Handler {
    private CurrentMeetupsFragment parentFragment;

    @Override
    public void handleMessage(Message msg) {
        if (msg.obj == null || msg.obj.equals("")) {
            return;
        }
        Gson gson = StaticFunctions.getGsonWithDateRegister();
        try {
            List<MeetUpEntity> meetUpEntities = gson.fromJson((String) msg.obj, new TypeToken<List<MeetUpEntity>>() {
            }.getType());
            parentFragment.setMeetUps(meetUpEntities);

        } catch (JsonSyntaxException e) {
            return;
        }
    }

    public void setParentFragment(CurrentMeetupsFragment parentFragment) {
        this.parentFragment = parentFragment;
    }
}