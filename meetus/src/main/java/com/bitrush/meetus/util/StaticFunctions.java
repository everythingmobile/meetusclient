package com.bitrush.meetus.util;

import android.app.Activity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserInfo;
import com.google.gson.*;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * Created by vivek on 12/20/14.
 */
public class StaticFunctions {
    private static DateFormat dfm = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa");
    public static String getRegionOfUser(Activity activity) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        int countryCode = getUserCountryCode(activity);

        return phoneNumberUtil.getRegionCodeForCountryCode(countryCode);
    }
    
    public static Long getUserPhoneNumber(Activity activity) {
        return Long.parseLong(SharedPreferencesUtil.read(activity, activity.getResources().getString(R.string.preferencePhoneNumber)));
    }
    
    public static int getUserCountryCode(Activity activity) {
        return Integer.parseInt(SharedPreferencesUtil.read(activity, activity.getResources().getString(R.string.preferenceCountryCode)));
    }

    public static String getUserName(Activity activity) {
        return SharedPreferencesUtil.read(activity, activity.getResources().getString(R.string.preferenceUserName));
    }

    public static String getAuthToken(Activity activity) {
        return SharedPreferencesUtil.read(activity, activity.getResources().getString(R.string.preferenceToken));
    }

    public static Gson getGsonWithDateRegister() {
        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });

        return builder.create();
    }

    public static UserInfo getCurrentUserInfo(Activity activity) {
        PhoneInfo phoneInfo = new PhoneInfo(getUserCountryCode(activity), getUserPhoneNumber(activity));
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName(StaticFunctions.getUserName(activity));
        userInfo.setPhoneInfo(phoneInfo);
        return userInfo;
    }

    public static String getDateWithCurrentTimeZone(Date date) {
        dfm.setTimeZone(TimeZone.getDefault());
        return dfm.format(date);
    }

    public static String getUrlForStaticMap(MeetUpEntity meetUpEntity) {
        String urlE = URLEncoder.encode(meetUpEntity.getMeetUpLocation().getDestinationLatitude() + "," + meetUpEntity.getMeetUpLocation().getDestinationLongitude());
        return "http://maps.google.com/maps/api/staticmap?center=" + urlE
                + "&zoom=15&size=500x300&maptype=roadmap&sensor=true&markers=color:red%7C" + urlE
                + "&key=" + "AIzaSyBXCr_NyVD66W2pkjUPVP7nNVq7Z2BmbSQ";
    }


    public static String getDescription(MeetUpEntity meetUp) {
        List<UserInfo> participants = meetUp.getParticipants();
        if (participants.size() == 0) {
            return "";
        }
        if (participants.size() == 1) {
            return "Join " + participants.get(0).getUserName() + " for a meetup";
        }
        if (participants.size() == 2)
            return participants.get(0).getUserName() + " and " + participants.get(1).getUserName() + " are meeting";
        return participants.get(0).getUserName() + "," + participants.get(1).getUserName() + " and " + (participants.size() - 2) + " others are meeting up";
    }
}
