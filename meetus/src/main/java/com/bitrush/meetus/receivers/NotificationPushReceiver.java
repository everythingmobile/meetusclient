package com.bitrush.meetus.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import android.util.Log;
import com.bitrush.meetus.services.MeetusNotificationService;

/**
 * @author ge3k on 27/1/15.
 */
public class NotificationPushReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("GCM_TEST","IN the receiver!!!");
        ComponentName comp = new ComponentName(context.getPackageName(),
                MeetusNotificationService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
