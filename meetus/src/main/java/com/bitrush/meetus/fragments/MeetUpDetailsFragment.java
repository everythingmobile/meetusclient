package com.bitrush.meetus.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.ViewTreeObserver;
import android.widget.*;
import at.markushi.ui.CircleButton;
import com.bitrush.meetus.MeetupInfoActivity;
import com.bitrush.meetus.MeetusNavigationActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.model.MeetUpEntityChanges;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.Communicator;
import com.squareup.picasso.Picasso;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class MeetUpDetailsFragment extends Fragment {

    private static MeetupInfoActivity parentActivity;
    private StartMeetUpMessage startMeetUpMessage;
    private View currentView;
    private EditText timeET;
    private EditText dateET;
    private TextView locationDescriptionTV;
    private TextView locationAddressTV;
    private String date;
    private ProgressDialog progressDialog;
    private ImageView staticMap;
    private boolean dateSet = false;
    private boolean timeSet = false;
    boolean initialisedStaticMap = false;
    private LinearLayout viewParticipantsTV;
    private String time;
    private DateFormat dfm = new SimpleDateFormat("dd MMM yyyy hh:mm aa");
    private CircleButton inviteButton;
    private TextView numberOfParticipantsTV;
    private TextView doneButton;
    private TextView startMeetUpButton;
    private Date originalDate;
    private MeetUpEntityChanges changes;
    private boolean paused = false;
    private static UserMeetUpRelationConfig config;
    private TextView viewCommentsTV;

    public MeetUpDetailsFragment() {
    }

    public static MeetUpDetailsFragment newInstance(MeetupInfoActivity meetupInfoActivity, StartMeetUpMessage startMeetUpMessage,
                                                    UserMeetUpRelationConfig config) {
        MeetUpDetailsFragment.parentActivity = meetupInfoActivity;
        MeetUpDetailsFragment.config = config;
        MeetUpDetailsFragment meetUpDetailsFragment = new MeetUpDetailsFragment();
        meetUpDetailsFragment.startMeetUpMessage = startMeetUpMessage;
        meetUpDetailsFragment.originalDate = startMeetUpMessage.getStartUpTime();
        return meetUpDetailsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        currentView = inflater.inflate(R.layout.fragment_meet_up_details, container, false);
        setViews();
        setViewValues();
        setListeners();
        return currentView;
    }

    private void setViewValues() {
        addStaticMap();
        setLocationDetails();
        setDateAndTimeETs();
        updateCount();
    }

    private void setDateAndTimeETs() {
        Date meetUpDate = startMeetUpMessage.getStartUpTime();
        if (meetUpDate == null) {
            return;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
        dateET.setText(dateFormat.format(meetUpDate));
        timeET.setText(timeFormat.format(meetUpDate));
    }

    private void setViews() {
        timeET = (EditText) currentView.findViewById(R.id.timeET);
        dateET = (EditText) currentView.findViewById(R.id.dateET);
        inviteButton = (CircleButton) currentView.findViewById(R.id.inviteButton);
        locationAddressTV = (TextView) currentView.findViewById(R.id.locationAddress);
        locationDescriptionTV = (TextView) currentView.findViewById(R.id.meetUpDescription);
        viewParticipantsTV = (LinearLayout) currentView.findViewById(R.id.viewParticipantsButton);
        numberOfParticipantsTV = (TextView) currentView.findViewById(R.id.numberOfParticipantsTV);
        doneButton = (TextView) currentView.findViewById(R.id.doneButton);
        staticMap = (ImageView) currentView.findViewById(R.id.staticMapView);
        startMeetUpButton = (TextView) currentView.findViewById(R.id.startMeetUpButton);
        viewCommentsTV = (TextView) currentView.findViewById(R.id.viewComments);
    }

    private void setLocationDetails() {
        if (startMeetUpMessage == null || startMeetUpMessage.getMeetUpLocation() == null || startMeetUpMessage.getMeetUpLocation().getLocationAddress() == null ||
                startMeetUpMessage.getMeetUpLocation().getLocationDescription() == null) {
            return;
        }
        locationAddressTV.setText(startMeetUpMessage.getMeetUpLocation().getLocationAddress());
        locationDescriptionTV.setText(startMeetUpMessage.getMeetUpLocation().getLocationDescription());
    }

    public void doneAddingMeetUp(String meetUpId) {
        progressDialog.hide();
        parentActivity.doneAddingMeetUp();
    }

    private void setListeners() {
        if (config.isUserAdmin) {
            setListenerForDatePicker();
            setListenerForTimePicker();
        }
        if (config.isGoing) {
            setListenerForStartUpButton();
        }
        setListenerForInviteButton();
        setListenerForDoneButton();
        setListenerForViewParticipantsTV();
        setListenerForViewCommentsTV();
    }

    private void setListenerForViewCommentsTV() {
        viewCommentsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.displayComments();
            }
        });
    }

    private void setListenerForStartUpButton() {
        startMeetUpButton.setVisibility(View.VISIBLE);
        startMeetUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parentActivity, MeetusNavigationActivity.class);
                intent.putExtra(MeetusNavigationActivity.INTENT_PARAM_START_UP_MESSAGE, startMeetUpMessage);
                intent.putExtra(MeetusNavigationActivity.INTENT_PARAM_MEETUP_ID, parentActivity.getMeetUpId());
                startActivity(intent);
                parentActivity.finish();
            }
        });
    }

    private void setListenerForDoneButton() {
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDetailsAndAddMeetUp();
            }
        });
    }

    private void checkDetailsAndAddMeetUp() {
        if (startMeetUpMessage.getStartUpTime() == null) {
            Toast.makeText(parentActivity, "Set meetup time", Toast.LENGTH_LONG).show();
        } else if (startMeetUpMessage.getInvitees() == null || startMeetUpMessage.getInvitees().size() == 0) {
            Toast.makeText(parentActivity, "Invite more people", Toast.LENGTH_LONG).show();
        } else {
            startDialog();
        }
    }

    private void startDialog() {
        if(parentActivity.isAddingMeetUp()) {
            startAddingMeetUpFlow();
        } else {
            startModifyingMeetUpFlow();
        }
    }

    private void startModifyingMeetUpFlow() {
        if (!initChanges()) {
            parentActivity.doneAddingMeetUp();
            return;
        }
        final MeetUpDetailsFragment thisF = this;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Communicator.updateMeetUp(changes, thisF);
                        progressDialog = ProgressDialog.show(parentActivity, "", "Updating MeetUp");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        parentActivity.doneAddingMeetUp();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
        builder.setMessage("Do you want to update this MeetUp?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    private boolean initChanges() {
        changes = new MeetUpEntityChanges();
        if (originalDate != startMeetUpMessage.getStartUpTime()) {
            changes.setStartUpTime(startMeetUpMessage.getStartUpTime());
        }
        if (parentActivity.getAddedUsers() != null && parentActivity.getAddedUsers().size() > 0) {
            changes.setAddedUsers(parentActivity.getAddedUsers());
        }
        if (parentActivity.getRemovedUsers() != null && parentActivity.getRemovedUsers().size() > 0) {
            changes.setRemovedUsers(parentActivity.getRemovedUsers());
        }
        changes.setMeetUpId(parentActivity.getMeetUpId());
        return changes.getStartUpTime() != null || changes.getAddedUsers() != null || changes.getRemovedUsers() != null;
    }

    private void startAddingMeetUpFlow() {
        final MeetUpDetailsFragment thisF = this;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Communicator.addMeetUp(startMeetUpMessage, thisF);
                        progressDialog = ProgressDialog.show(parentActivity, "", "Adding MeetUp");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        parentActivity.doneAddingMeetUp();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
        builder.setMessage("Do you want to add this MeetUp?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    private void setListenerForViewParticipantsTV() {
        viewParticipantsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeetUpDetailsFragment.parentActivity.showInviteFragment();
            }
        });
    }

    private void setListenerForInviteButton() {
        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeetUpDetailsFragment.parentActivity.showInviteFragment();
            }
        });
    }

    private void setListenerForTimePicker() {
        final MeetUpDetailsFragment thisF = this;
        timeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment picker = TimePickerFragment.newInstance(thisF);
                picker.show(getFragmentManager(), "timePicker");
            }
        });
    }

    private void setListenerForDatePicker() {
        final MeetUpDetailsFragment thisF = this;
        dateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment picker = DatePickerFragment.newInstance(thisF);
                picker.show(getFragmentManager(), "datePicker");
            }
        });
    }

    private void updateCount() {
        if(numberOfParticipantsTV != null) {
            if(startMeetUpMessage.getInvitees() == null) {
                startMeetUpMessage.setInvitees(new ArrayList<UserInfo>());
            }
            numberOfParticipantsTV.setText(String.valueOf(startMeetUpMessage.getInvitees().size()));
        }
    }

    private void addStaticMap() {
        final ImageView imageView = (ImageView) currentView.findViewById(R.id.staticMapView);
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                drawStaticMapUsingPicasso(imageView);
                return true;
            }
        });

    }

    public void setDate(String date) {
        this.date = date;
        dateSet = true;
        EditText editText = (EditText) currentView.findViewById(R.id.dateET);
        editText.setText(date);
        if(timeSet) {
            setMeetUpStartTime();
        }
    }

    private void setMeetUpStartTime() {
        dfm.setTimeZone(TimeZone.getDefault());
        long uTime;
        try {
            uTime = dfm.parse(date + " " + time).getTime();
        } catch (ParseException p) {
            uTime = new Date().getTime();
        }
        startMeetUpMessage.setStartUpTime(new Date(uTime));
    }

    public void setTime(String time) {
        this.time = time;
        timeSet = true;
        EditText editText = (EditText) currentView.findViewById(R.id.timeET);
        editText.setText(time);
        if(dateSet) {
            setMeetUpStartTime();
        }
    }

    private void drawStaticMapUsingPicasso(ImageView imageView) {
        if(imageView.getDrawable() != null) {
            return;
        }
        int height = imageView.getMeasuredHeight()/2;
        int width = imageView.getMeasuredWidth()/2;
        String size = Integer.toString(width) + 'x' + Integer.toString(height);
        Picasso picasso = Picasso.with(parentActivity);
        picasso.load(getUrlForStaticMap(startMeetUpMessage, size)).into(imageView);
        initialisedStaticMap = true;
    }

    private String getUrlForStaticMap(StartMeetUpMessage startMeetUpMessage, String size) {
        String urlE = URLEncoder.encode(startMeetUpMessage.getMeetUpLocation().getDestinationLatitude() + "," + startMeetUpMessage.getMeetUpLocation().getDestinationLongitude());
        return "http://maps.google.com/maps/api/staticmap?center=" + urlE
                +"&scale=2&zoom=15&size="+ size +"&maptype=roadmap&sensor=true&markers=color:red%7C" + urlE
                + "&key=" + "AIzaSyBXCr_NyVD66W2pkjUPVP7nNVq7Z2BmbSQ";
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void doneUpdatingMeetUp() {
        progressDialog.hide();
        parentActivity.doneAddingMeetUp();
    }
}
