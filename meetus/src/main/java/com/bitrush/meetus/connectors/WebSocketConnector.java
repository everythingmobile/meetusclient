package com.bitrush.meetus.connectors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.config.WebSocketConfig;
import com.bitrush.meetus.handlers.UserLocationWebSocketHandler;
import com.bitrush.meetus.model.MeetusInitMessage;
import com.bitrush.meetus.model.MeetusUserDetail;
import com.google.gson.Gson;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

import java.util.concurrent.ExecutionException;


public class WebSocketConnector implements
        MeetusConstants {

    private static final String INIT_HEADER = "init";
    private final UserLocationWebSocketHandler userLocationWebSocketHandler;
    private final WebSocketConfig webSocketConfig;
    private final CompletedCallback mClosedCallback;
    private WebSocket webSocket;
    private AsyncHttpClient.WebSocketConnectCallback mCallback;
    private BroadcastReceiver connectionReceiver;
    private WebSocket.StringCallback mStringCallback;

    public WebSocketConnector(final UserLocationWebSocketHandler userLocationWebSocketHandler, WebSocketConfig webSocketConfig) {
        this.userLocationWebSocketHandler = userLocationWebSocketHandler;
        this.webSocketConfig = webSocketConfig;
        mCallback = new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, WebSocket webSocket) {
                if (ex != null) {
                    Log.e(MeetusConstants.APPTAG, "Error on Completed for web socket", ex);
                    Log.e(MeetusConstants.APPTAG, ex.getLocalizedMessage());
                    if (webSocket != null) {
                        webSocket.close();
                    }
                } else {
                    Log.i(MeetusConstants.APPTAG, "On Completed called for websocket");
                }
            }
        };

        mStringCallback = new WebSocket.StringCallback() {
            @Override
            public void onStringAvailable(String s) {
                Log.i(APPTAG, "*** Data available is ***" + s);
                Gson gson = new Gson();
                MeetusUserDetail message = gson.fromJson(s, MeetusUserDetail.class);
                if (userLocationWebSocketHandler != null) {
                    Message message1 = userLocationWebSocketHandler.obtainMessage();
                    message1.what = MeetusConstants.MESSAGE_KEY_LOCATION;
                    Bundle messageBundle = new Bundle();
                    messageBundle.putSerializable(MeetusConstants.LOCATION_CHANGED_SERIALIZABLE_KEY, message);
                    message1.setData(messageBundle);
                    userLocationWebSocketHandler.sendMessage(message1);
                    Log.d(APPTAG, message.toString());
                }
            }
        };

        mClosedCallback = new CompletedCallback() {
            @Override
            public void onCompleted(Exception e) {
                Log.d(APPTAG, "Websocket closing now....");
                if (e != null) {
                    Log.e(APPTAG, "*** ERROR **" + e.getLocalizedMessage());
                }
            }
        };


        this.connectionReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);

                final android.net.NetworkInfo wifi = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                final android.net.NetworkInfo mobile = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                if (wifi.isAvailable() || mobile.isAvailable()) {
                    Log.i(MeetusConstants.APPTAG, "Connecting as at least one amongst wifi or celluar network is available");
                    WebSocketConnector.this.connect("ws://ec2-52-74-140-186.ap-southeast-1.compute.amazonaws.com:4000/ws");
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.userLocationWebSocketHandler.getParentActivity().registerReceiver(connectionReceiver, filter);
    }

    public void connect(String serverUrl) {
        if (webSocketConfig == null) {
            Log.e(MeetusConstants.APPTAG, "ERROR: Websocket config cannot be null.");
            return;
        }
        if (serverUrl == null) {
            Log.e(MeetusConstants.APPTAG, "Server URL cannot be null");
            return;
        }
        Future<WebSocket> client = AsyncHttpClient.getDefaultInstance().websocket(serverUrl, null, mCallback);
        try {
            webSocket = client.get();
            webSocket.setStringCallback(mStringCallback);
            webSocket.setClosedCallback(mClosedCallback);
            sendInitMessage(webSocket, webSocketConfig);
        } catch (InterruptedException e) {
            Log.e(APPTAG, "Interrupted exception Cannot get the websocket" + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.e(APPTAG, " Execution exception : Cannot get the websocket " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void sendInitMessage(WebSocket webSocket, WebSocketConfig config) {
        if (webSocket != null) {

            MeetusInitMessage initMessage = new MeetusInitMessage();
            initMessage.setMessageHead(INIT_HEADER);
            initMessage.setMeetUpId(config.getMeetupId());
            initMessage.setUserCountryCode(config.getUserContryCode());
            initMessage.setUserPhoneNumber(config.getUserPhoneNumber());
            initMessage.setUserToken(config.getToken());
            Gson gson = new Gson();
            String jsonObject = gson.toJson(initMessage);

            Log.i(APPTAG, " Init message JSON is  ************* \n" + jsonObject + " \n******************");

            if (jsonObject != null) {
                webSocket.send(jsonObject);
            }
            Log.d(APPTAG, "Successfully sent the JSON object");
        }

    }

    public void sendWebSocketMessage(String message) {
        if (webSocket == null) {
            Log.e(MeetusConstants.APPTAG, "Websocket is null cannot write message: " + message);
            return;
        }
        webSocket.send(message);
    }


    public Handler getUserLocationWebSocketHandler() {
        return userLocationWebSocketHandler;
    }

    public boolean isWebSocketUp() {
        return (webSocket != null);
    }

}
