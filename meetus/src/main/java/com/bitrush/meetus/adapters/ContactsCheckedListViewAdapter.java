package com.bitrush.meetus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.bitrush.meetus.R;
import com.bitrush.meetus.config.UserMeetUpRelationConfig;
import com.bitrush.meetus.fragments.InviteParticipantsFragment;
import com.bitrush.meetus.model.ContactInfo;
import com.bitrush.meetus.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 1/19/15.
 */
public class ContactsCheckedListViewAdapter extends ArrayAdapter<ContactInfo> {
    private final UserMeetUpRelationConfig config;
    private final ListView thisListView;
    private List<UserInfo> participants;
    private List<UserInfo> contacts;
    private Context context;
    private List<ContactInfo> contactInfoList;
    private InviteParticipantsFragment parentFragment;

    static class ViewHolder {
        RadioButton isCheckedRB;
        TextView contactViewTV;
    }

    public ContactsCheckedListViewAdapter(Context context, int resource, List<UserInfo> participants, List<UserInfo> contacts, ListView thisListView,
                                          InviteParticipantsFragment parentFragment, UserMeetUpRelationConfig config) {
        super(context, resource);
        this.contacts = contacts;
        this.config = config;
        this.context = context;
        this.thisListView = thisListView;
        this.parentFragment = parentFragment;
        this.participants = participants;
        setContactInfoList();
    }

    private void setContactInfoList() {
        contactInfoList = new ArrayList<>();
        for (UserInfo user : contacts) {
            int position = participants.indexOf(user);
            ContactInfo contactInfo = new ContactInfo(user, false);
            if (position != -1) {
                contactInfo.setInvited(true);
            }
            contactInfoList.add(contactInfo);
        }
    }

    @Override
    public int getPosition(ContactInfo item) {
        return contactInfoList.indexOf(item);
    }

    @Override
    public ContactInfo getItem(int position) {
        return contactInfoList.get(position);
    }

    @Override
    public int getCount() {
        return contactInfoList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.invite_list_item, null, false);

            viewHolder = new ViewHolder();
            viewHolder.contactViewTV = (TextView) convertView.findViewById(R.id.contactName);
            viewHolder.isCheckedRB = (RadioButton) convertView.findViewById(R.id.isCheckedRB);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.contactViewTV.setText(getItem(position).getUserInfo().getUserName());
        viewHolder.isCheckedRB.setChecked(getItem(position).isInvited());
        if (config.isGoing) {
            setListener(convertView, viewHolder, position);
        }
        return convertView;
    }

    private void setListener(View convertView,final ViewHolder viewHolder, final int position) {
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.isCheckedRB.isChecked()) {
                    contactInfoList.get(position).setInvited(false);
                    participants.remove(contactInfoList.get(position).getUserInfo());
                    parentFragment.removedUser(contactInfoList.get(position).getUserInfo());
                } else {
                    contactInfoList.get(position).setInvited(true);
                    participants.add(contactInfoList.get(position).getUserInfo());
                    parentFragment.addedUser(contactInfoList.get(position).getUserInfo());
                }
                viewHolder.isCheckedRB.setChecked(contactInfoList.get(position).isInvited());
                parentFragment.participantsChanged(true);
            }
        });
    }

    public void participantsChanged() {
        setContactInfoList();
        notifyDataSetInvalidated();
        GoingCheckedListViewAdapter.setListViewHeightBasedOnChildren(thisListView);
    }
}
