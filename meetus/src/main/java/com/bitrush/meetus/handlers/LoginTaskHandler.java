package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.LoginFragment;
import com.bitrush.meetus.model.RegistrationVerficationMessage;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by vivek on 9/14/14.
 */
public class LoginTaskHandler extends Handler {
    private LoginFragment loginFragment;
    @Override
    public void handleMessage(Message msg) {
        Gson gson = new Gson();
        try {
            RegistrationVerficationMessage registrationVerficationMessage = gson.fromJson((String)msg.obj, RegistrationVerficationMessage.class);
            loginFragment.setLoginStatus(registrationVerficationMessage.getRegistered());
        } catch (JsonSyntaxException e) {
            loginFragment.setLoginStatus(false);
        }

    }

    public void setLoginFragment(LoginFragment loginFragment) {
        this.loginFragment = loginFragment;
    }
}