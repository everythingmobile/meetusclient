package com.bitrush.meetus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.*;
import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.CountriesDialog;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.SharedPreferencesUtil;

public class LoginFragment extends Fragment {

    private static LoginRegisterActivity parentActivity;
    private static String finalPhoneNumber;
    private static int finalCountryCode;
    private Spinner spinner;
    private String[] countryCodes;
    private CountriesDialog countriesDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        View finalView = inflater.inflate(R.layout.fragment_login, container, false);
        countryCodes = getResources().getStringArray(R.array.CountryNames);
        spinner = (Spinner) finalView.findViewById(R.id.country_list);
        setBuilder();
        setOnClickListeners(finalView);
        return finalView;
    }

    private void setBuilder() {
        countriesDialog = new CountriesDialog(parentActivity);
        countriesDialog.initializeBuilder(countryCodes, spinner);
    }

    private void setOnClickListeners(final View view) {
        Button doneButton = (Button)view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Verification / Sanitization
                EditText phoneNumber = (EditText) view.findViewById(R.id.loginPhoneNumber);
                PhoneInfo phoneInfo;
                finalPhoneNumber = phoneNumber.getText().toString();
                finalCountryCode = countriesDialog.getCurrentCountryCode();
                try {
                    phoneInfo = new PhoneInfo(finalCountryCode, Long.parseLong(finalPhoneNumber));
                }
                catch (NumberFormatException e) {
                    return;
                }
                UserInfo userInfo = new UserInfo();
                userInfo.setPhoneInfo(phoneInfo);
                loginWith(userInfo);
            }
        });
    }

    private void loginWith(UserInfo phone) {
        Communicator.login(phone, this);
    }

    public static LoginFragment newInstance(LoginRegisterActivity loginRegisterActivity) {
        LoginFragment.parentActivity = loginRegisterActivity;
        return new LoginFragment();
    }

    public void setLoginStatus(boolean status) {
        if(!status) {
            Toast.makeText(parentActivity.getApplicationContext(), "Sorry this Phone Number in not registered", Toast.LENGTH_LONG).show();
            return;
        }
        SharedPreferencesUtil.write(parentActivity, parentActivity.getResources().getString(R.string.preferenceCountryCode), Integer.toString(finalCountryCode));
        SharedPreferencesUtil.write(parentActivity, parentActivity.getResources().getString(R.string.preferencePhoneNumber), finalPhoneNumber);
        Toast.makeText(parentActivity.getApplicationContext(), "Logging in", Toast.LENGTH_LONG).show();
        parentActivity.startVerification();
    }
}
