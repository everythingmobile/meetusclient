package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.MeetUpDetailsFragment;

/**
 * Created by vivek on 2/28/15.
 */
public class UpdateMeetUpHandler extends Handler {
    private MeetUpDetailsFragment meetUpDetailsFragment;

    public UpdateMeetUpHandler(MeetUpDetailsFragment meetUpDetailsFragment) {
        this.meetUpDetailsFragment = meetUpDetailsFragment;
    }

    @Override
    public void handleMessage(Message msg) {
        String message = (String)msg.obj;
        if(message!=null && message.equals("")) {
            meetUpDetailsFragment.doneUpdatingMeetUp();
        }
    }
}
