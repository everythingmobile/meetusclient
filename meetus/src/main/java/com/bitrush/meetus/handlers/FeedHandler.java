package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import com.bitrush.meetus.fragments.FeedDisplayFragment;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.util.StaticFunctions;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 9/20/14.
 */
public class FeedHandler extends Handler{
    private FeedDisplayFragment parentFragment;
    @Override
    public void handleMessage(Message msg) {
        if(msg.obj == null || msg.obj.equals("")) {
            return;
        }
        Gson gson = StaticFunctions.getGsonWithDateRegister();
        try {
            Type listType = new TypeToken<ArrayList<MeetUpEntity>>(){}.getType();
            List<MeetUpEntity> meetUpEntities = gson.fromJson((String)msg.obj, listType);
            parentFragment.setMeetUps(meetUpEntities);

        } catch (JsonSyntaxException e) {
            return;
        }
    }

    public void setParentFragment(FeedDisplayFragment parentFragment) {
        this.parentFragment = parentFragment;
    }


}
