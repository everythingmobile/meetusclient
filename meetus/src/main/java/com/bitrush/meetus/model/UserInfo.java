package com.bitrush.meetus.model;

import java.io.Serializable;

/**
 * Created by vivek on 9/13/14.
 */
public class UserInfo implements Serializable, Comparable {
    private PhoneInfo phoneInfo;
    private String userName;
    private String emailId;
    private boolean registered;

    public UserInfo() {
    }

    public UserInfo(PhoneInfo userPhoneNumber, String userName, String emailId) {
        this.phoneInfo = userPhoneNumber;
        this.userName = userName;
        this.emailId = emailId;
    }

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserInfo{");
        sb.append("phoneInfo=").append(phoneInfo);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", emailId='").append(emailId).append('\'');
        sb.append(", registered=").append(registered);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        UserInfo userInfo = (UserInfo) o;
        return this.getPhoneInfo().compareTo(userInfo.getPhoneInfo());
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        UserInfo userInfo = (UserInfo) obj;
        return this.getPhoneInfo().getCountryCode() == userInfo.getPhoneInfo().getCountryCode() &&
                this.getPhoneInfo().getPhoneNumber() == userInfo.getPhoneInfo().getPhoneNumber();
    }
}
