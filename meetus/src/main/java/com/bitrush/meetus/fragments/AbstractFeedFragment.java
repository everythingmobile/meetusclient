package com.bitrush.meetus.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitrush.meetus.MeetUpsActivity;
import com.bitrush.meetus.R;
import com.bitrush.meetus.adapters.MeetUpRecycleAdapter;
import com.bitrush.meetus.model.MeetUpEntity;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.util.PullToRefreshHelper;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

/**
 * Created by ge3k on 16/05/15.
 */
public abstract class AbstractFeedFragment extends Fragment {

    public static MeetUpsActivity parentActivity;
    public static MeetUpRecycleAdapter meetUpsAdapter;
    private String meetupsType;

    private SwipeRefreshLayout swipeRefreshLayout;
    private PullToRefreshHelper mHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mHelper = new PullToRefreshHelper(parentActivity);
        swipeRefreshLayout = (SwipeRefreshLayout) inflater.inflate(R.layout.feed_meet_up_recycle_display, container, false);

        RecyclerView recyclerView = (RecyclerView) swipeRefreshLayout.findViewById(R.id.meetups_recycle);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(parentActivity.getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        meetUpsAdapter = new MeetUpRecycleAdapter(parentActivity.getApplicationContext(), this);
        recyclerView.setAdapter(meetUpsAdapter);
        FloatingActionButton fab = (FloatingActionButton) swipeRefreshLayout.findViewById(R.id.fab);
        fab.attachToRecyclerView(recyclerView);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mHelper.performAsyncLoad(meetUpsAdapter, swipeRefreshLayout, meetupsType);
            }
        });
        getMeetUps();
        return swipeRefreshLayout;
    }

    public abstract void getMeetUps();


    public void setMeetUps(List<MeetUpEntity> meetUpEntities) {
        meetUpsAdapter.setMeetUps(meetUpEntities);
        meetUpsAdapter.notifyDataSetChanged();
    }

    public void popUp(Intent intent) {
        startActivity(intent);
    }

    public abstract void showMeetUpInfo(StartMeetUpMessage startMeetUpMessage, String message);

    public String getMeetupsType() {
        return meetupsType;
    }

    public void setMeetupsType(String meetupsType) {
        this.meetupsType = meetupsType;
    }
}
