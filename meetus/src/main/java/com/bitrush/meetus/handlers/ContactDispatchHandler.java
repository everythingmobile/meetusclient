package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.bitrush.meetus.LoginRegisterActivity;
import com.bitrush.meetus.model.PhoneInfo;
import com.bitrush.meetus.model.UserInfo;
import com.bitrush.meetus.util.Communicator;
import com.bitrush.meetus.util.SharedPreferencesUtil;

import java.util.List;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * @author ge3k on 20/9/14.
 */
public class ContactDispatchHandler extends Handler{
    private LoginRegisterActivity parentActivity;

    public ContactDispatchHandler(LoginRegisterActivity parentActivity) {
        this.parentActivity = parentActivity;
    }

    @Override
    public void handleMessage(Message msg) {
        List<UserInfo> userInfoList = (List<UserInfo>) msg.obj;
        UserInfo currentUserInfo = new UserInfo();
        String currentUserPhoneNumber = SharedPreferencesUtil.read(parentActivity, "sharedPreference.phoneNumber");
        String currentCountryCode = SharedPreferencesUtil.read(parentActivity, "sharedPreference.countryCode");
        if (currentUserPhoneNumber.isEmpty() || currentCountryCode.isEmpty()) {
            TelephonyManager tm = (TelephonyManager)parentActivity.getSystemService(TELEPHONY_SERVICE);
            currentUserPhoneNumber = tm.getLine1Number();
        }
        PhoneInfo currentPhoneInfo = new PhoneInfo(Integer.parseInt(currentCountryCode), Long.parseLong(currentUserPhoneNumber));
        currentUserInfo.setPhoneInfo(currentPhoneInfo);
        Communicator.verifyContacts(currentUserInfo, userInfoList, parentActivity);
    }
}
