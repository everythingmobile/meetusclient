package com.bitrush.meetus.model;

import java.util.List;

public class UserAndContacts {
    private UserInfo userInfo;
    private List<UserInfo> userContacts;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<UserInfo> getUserContacts() {
        return userContacts;
    }

    public void setUserContacts(List<UserInfo> userContacts) {
        this.userContacts = userContacts;
    }
}
