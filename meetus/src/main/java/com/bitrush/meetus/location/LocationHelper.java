package com.bitrush.meetus.location;

import android.app.Dialog;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.bitrush.meetus.MeetusConstants;
import com.bitrush.meetus.MeetusNavigationActivity;
import com.bitrush.meetus.asynctasks.WebSocketBackgroundTask;
import com.bitrush.meetus.handlers.UserLocationWebSocketHandler;
import com.bitrush.meetus.model.MeetusUserDetail;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.util.StaticFunctions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

public class LocationHelper implements AsyncHttpClient.WebSocketConnectCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, MeetusConstants {
//Done based on https://stackoverflow.com/questions/24611977/android-locationclient-class-is-deprecated-but-used-in-documentation
    private Location currentLocation;
    private GoogleApiClient mLocationClient;
    private LocationRequest locationRequest;
    private MeetusNavigationActivity parentActivity;
    private UserLocationWebSocketHandler webSocketDetailsHandler;
    private WebSocketBackgroundTask webSocketBackgroundTask;

    public LocationHelper(final MeetusNavigationActivity parentActivity, StartMeetUpMessage startMeetUpMessage, String meetUpId) {
        this.parentActivity = parentActivity;
        createLocationRequest();
        initialiseLocationClient(parentActivity);
        initialiseWebSocketHandler(parentActivity);
        startWebSocketBackgroundTask(parentActivity, startMeetUpMessage, meetUpId);
    }

    private void startWebSocketBackgroundTask(MeetusNavigationActivity parentActivity, StartMeetUpMessage startMeetUpMessage, String meetUpId) {
        webSocketBackgroundTask = new WebSocketBackgroundTask(webSocketDetailsHandler);
        webSocketBackgroundTask.execute(startMeetUpMessage, meetUpId, parentActivity);
    }

    private void initialiseWebSocketHandler(final MeetusNavigationActivity parentActivity) {
        webSocketDetailsHandler = new UserLocationWebSocketHandler();
        webSocketDetailsHandler.setParentActivity(parentActivity);
    }

    private void initialiseLocationClient(MeetusNavigationActivity parentActivity) {
        mLocationClient = new GoogleApiClient.Builder(parentActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationClient.connect();
        Log.i(APPTAG,"Sent connect from location helper");
    }

    private void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setFastestInterval(FAST_INTERVAL_CEILING_IN_MILLISECONDS);
        locationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT);
    }

    private Location getLocation() {
        if (servicesConnected()) {
            return LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
        } else {
            Log.d(APPTAG,"Unable to find the location!!!");
            return null;
        }
    }

    private void startPeriodicUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, locationRequest, this);
    }

    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(parentActivity);
        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d(APPTAG, "Google play services available");
            return true;
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, parentActivity, 0);
            if (dialog != null) {
                MeetusNavigationActivity.ErrorDialogFragment errorFragment = new MeetusNavigationActivity.ErrorDialogFragment();
                errorFragment.setDialog(dialog);
                errorFragment.show(parentActivity.getSupportFragmentManager(), APPTAG);
            }
            return false;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(parentActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.d(APPTAG, "An error occurred when connecting to location services.", e);
            }
        } else {
            parentActivity.showErrorDialog(connectionResult.getErrorCode());
        }
    }

    @Override
    public void onCompleted(Exception ex, WebSocket webSocket) {
        if (ex != null ) {
            Log.e(APPTAG,ex.getLocalizedMessage());
            if (webSocket != null) {
                webSocket.close();
            }
        } else {
            Log.i(APPTAG,"On Completed called for websocket");
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(APPTAG,"Connected to location services");
        currentLocation = getLocation();
        startPeriodicUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(APPTAG,"Connection has been suspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        parentActivity.locationChangedForUser(location);
        if (webSocketBackgroundTask.isWebSocketAvailable()) {
            Log.i(APPTAG,"Writing to the websocket for this user's location changed");
            MeetusUserDetail regularMessage = new MeetusUserDetail();
            regularMessage.setId(StaticFunctions.getUserName(parentActivity));
            regularMessage.setLatitude(Double.toString(currentLocation.getLatitude()));
            regularMessage.setLongitude(Double.toString(currentLocation.getLongitude()));
            Gson gson = new Gson();
            String jsonString  = gson.toJson(regularMessage);
            webSocketBackgroundTask.writeData(jsonString);
        } else {
            Log.i(APPTAG,"The websocket is null. Therefore we are not sending anything!!");
        }
    }


    public Handler getWebSocketDetailsHandler() {
        return webSocketDetailsHandler;
    }

    public void setWebSocketDetailsHandler(UserLocationWebSocketHandler webSocketDetailsHandler) {
        this.webSocketDetailsHandler = webSocketDetailsHandler;
    }

    public MeetusNavigationActivity getParentActivity() {
        return parentActivity;
    }

    public void setParentActivity(MeetusNavigationActivity parentActivity) {
        this.parentActivity = parentActivity;
    }

    public LocationRequest getLocationRequest() {
        return locationRequest;
    }

    public void setLocationRequest(LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
    }

    public GoogleApiClient getmLocationClient() {
        return mLocationClient;
    }

    public void setmLocationClient(GoogleApiClient mLocationClient) {
        this.mLocationClient = mLocationClient;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }
}
