package com.bitrush.meetus.util;

import android.location.Location;

import com.bitrush.meetus.MeetusConstants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by ge3k on 14/9/14.
 */
public class MeetusLocationUtil implements MeetusConstants {
    public static LatLngBounds calculateBoundsWithCenter(LatLng myLatLng) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        double lngDifference = calculateLatLngOffset(myLatLng, false);
        LatLng east = new LatLng(myLatLng.latitude, myLatLng.longitude + lngDifference);
        builder.include(east);
        LatLng west = new LatLng(myLatLng.latitude, myLatLng.longitude - lngDifference);
        builder.include(west);
        double latDifference = calculateLatLngOffset(myLatLng, true);
        LatLng north = new LatLng(myLatLng.latitude + latDifference, myLatLng.longitude);
        builder.include(north);
        LatLng south = new LatLng(myLatLng.latitude - latDifference, myLatLng.longitude);
        builder.include(south);

        return builder.build();
    }

    public static double calculateLatLngOffset(LatLng myLatLng, boolean bLatOffset) {
        double latLngOffset = OFFSET_CALCULATION_INIT_DIFF;
        float desiredOffsetInMeters = MEETUS_RADIUS;
        float[] distance = new float[1];
        boolean foundMax = false;
        double foundMinDiff = 0;
        do {
            if (bLatOffset) {
                Location.distanceBetween(myLatLng.latitude, myLatLng.longitude, myLatLng.latitude
                        + latLngOffset, myLatLng.longitude, distance);
            } else {
                Location.distanceBetween(myLatLng.latitude, myLatLng.longitude, myLatLng.latitude,
                        myLatLng.longitude + latLngOffset, distance);
            }
            float distanceDiff = distance[0] - desiredOffsetInMeters;
            if (distanceDiff < 0) {
                if (!foundMax) {
                    foundMinDiff = latLngOffset;
                    latLngOffset *= 2;
                } else {
                    double tmp = latLngOffset;
                    latLngOffset += (latLngOffset - foundMinDiff) / 2;
                    foundMinDiff = tmp;
                }
            } else {
                latLngOffset -= (latLngOffset - foundMinDiff) / 2;
                foundMax = true;
            }
        } while (Math.abs(distance[0] - desiredOffsetInMeters) > OFFSET_CALCULATION_ACCURACY);
        return latLngOffset;
    }

    public static void zoomToLocation (com.bitrush.meetus.model.Location location, GoogleMap googleMap) {
        LatLng currentLatLng = new LatLng(Double.parseDouble(location.getDestinationLatitude()), Double.parseDouble(location.getDestinationLongitude()));
        LatLngBounds bounds = MeetusLocationUtil.calculateBoundsWithCenter(currentLatLng);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
    }
}
