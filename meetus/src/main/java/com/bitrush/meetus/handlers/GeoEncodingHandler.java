package com.bitrush.meetus.handlers;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.bitrush.meetus.fragments.PlacesAutoCompleteFragment;
import com.bitrush.meetus.model.Location;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 12/28/14.
 */
public class GeoEncodingHandler extends Handler {
    private PlacesAutoCompleteFragment placesAutoCompleteFragment;

    public GeoEncodingHandler(PlacesAutoCompleteFragment placesAutoCompleteFragment) {
        this.placesAutoCompleteFragment = placesAutoCompleteFragment;
    }

    @Override
    public void handleMessage(Message msg) {
        JSONObject jsonObject = new JSONObject();
        Location location = new Location();
        try {
            jsonObject = new JSONObject((String) msg.obj);


            double lng = jsonObject.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lng");
            double lat = jsonObject.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lat");
            String name = jsonObject.getJSONObject("result").getString("name");

            location.setDestinationLongitude(Double.toString(lng));
            location.setDestinationLatitude(Double.toString(lat));
            location.setLocationDescription(name);
        } catch (JSONException e) {
            Log.d("com.bitrush.meetus", "JSON parsing Exception");
        }
        placesAutoCompleteFragment.markMeetUpLocation(location);
    }
}
