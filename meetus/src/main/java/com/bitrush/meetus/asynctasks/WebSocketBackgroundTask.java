package com.bitrush.meetus.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.bitrush.meetus.MeetusNavigationActivity;
import com.bitrush.meetus.config.WebSocketConfig;
import com.bitrush.meetus.config.WebSocketConfigBuilder;
import com.bitrush.meetus.connectors.WebSocketConnector;
import com.bitrush.meetus.handlers.UserLocationWebSocketHandler;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.util.StaticFunctions;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.http.WebSocket;
import com.bitrush.meetus.MeetusConstants;

public class WebSocketBackgroundTask extends
        AsyncTask<Object, Void, Void> implements
        MeetusConstants {

    private Future<WebSocket> client;
    private UserLocationWebSocketHandler websocketDetailsHandler;
    private WebSocketConnector connector;
    private static final String webSocketUrl = "ws://ec2-52-74-140-186.ap-southeast-1.compute.amazonaws.com:4000/ws";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public WebSocketBackgroundTask(UserLocationWebSocketHandler websocketDetailsHandler) {
        this.websocketDetailsHandler = websocketDetailsHandler;
    }

    @Override
    protected Void doInBackground(Object... params) {
        StartMeetUpMessage startMeetUpMessage = (StartMeetUpMessage) params[0];
        String meetUpId = (String) params[1];
        MeetusNavigationActivity parentActivity = (MeetusNavigationActivity) params[2];
        String token = StaticFunctions.getAuthToken(parentActivity);
        String userName = StaticFunctions.getUserName(parentActivity);
        int userCountryCode = StaticFunctions.getUserCountryCode(parentActivity);
        long userPhoneNumber = StaticFunctions.getUserPhoneNumber(parentActivity);

        Log.i(MeetusConstants.APPTAG, "Performing background task....");
        WebSocketConfig webSocketConfig = new WebSocketConfigBuilder().withCountryCode(userCountryCode).withMeetupId(meetUpId)
                .withUserPhoneNumber(userPhoneNumber).withUserName(userName).withToken(token).build();
        connector = new WebSocketConnector(websocketDetailsHandler, webSocketConfig);
        connector.connect(webSocketUrl);
        return null;
    }

    public void writeData(String data) {
        connector.sendWebSocketMessage(data);
    }

    public boolean isWebSocketAvailable() {
        return connector != null && connector.isWebSocketUp();
    }

}
