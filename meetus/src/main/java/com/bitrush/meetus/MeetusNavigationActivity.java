package com.bitrush.meetus;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.bitrush.meetus.location.LocationHelper;
import com.bitrush.meetus.model.MapUser;
import com.bitrush.meetus.model.MeetusUserDetail;
import com.bitrush.meetus.model.StartMeetUpMessage;
import com.bitrush.meetus.util.MeetusLocationUtil;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import java.util.*;


public class MeetusNavigationActivity extends FragmentActivity implements MeetusConstants {

    public static final String INTENT_PARAM_START_UP_MESSAGE = "startup_message";
    public static final String INTENT_PARAM_MEETUP_ID = "meetup_id";
    private GoogleMap mMap;
    private Location currentLocation;
    private boolean hasSetUpInitialLocation = false;
    private LocationHelper locationHelper;
    private Map<String,MapUser> userMap;
    private List<Integer> colors;
    private StartMeetUpMessage startMeetUpMessage;
    private String meetUpId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        userMap = new HashMap<>();
        Intent intent = getIntent();
        startMeetUpMessage = intent.getParcelableExtra(INTENT_PARAM_START_UP_MESSAGE);
        meetUpId = intent.getStringExtra(INTENT_PARAM_MEETUP_ID);
        setUpMapIfNeeded();
        Log.i(APPTAG, "Initial Map has been set up. New staring location helper");
        colors = new ArrayList<>(Arrays.asList(Color.BLUE, Color.BLACK, Color.MAGENTA, Color.CYAN, Color.YELLOW));
        locationHelper = new LocationHelper(this, startMeetUpMessage, meetUpId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        if (currentLocation == null) {
            LatLng destinationLatLng = new LatLng(Double.parseDouble(startMeetUpMessage.getMeetUpLocation().getDestinationLatitude()),
                    Double.parseDouble(startMeetUpMessage.getMeetUpLocation().getDestinationLongitude()));
            mMap.addMarker(new MarkerOptions()
                    .position(destinationLatLng)
                    .title("Destination")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_marker)));
            Log.i(APPTAG,"Current location is null while setting map");
        }
        else {
            mMap.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),
                    currentLocation.getLongitude())).title("Your location"));
        }
    }

    private void updateZoom(LatLng myLatLng) {
        LatLngBounds bounds = MeetusLocationUtil.calculateBoundsWithCenter(myLatLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
    }

    public void showErrorDialog(int errorCode) {
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode, this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

        if (errorDialog != null) {
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();
            errorFragment.setDialog(errorDialog);
            errorFragment.show(getSupportFragmentManager(),APPTAG);
        }
    }

    public static class ErrorDialogFragment extends DialogFragment {
        private Dialog mDialog;
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    public void locationChangedForUser(Location changedLocation) {
        if (changedLocation != null) {
            currentLocation = changedLocation;
            /*mMap.addMarker(new MarkerOptions().position(new LatLng(changedLocation.getLatitude(),
                    changedLocation.getLongitude())).title("Your location"));*/

            LatLng myLatLng = new LatLng(changedLocation.getLatitude(), changedLocation.getLongitude());
            if (!hasSetUpInitialLocation) {
                // Zoom to the current location.
                updateZoom(myLatLng);
                hasSetUpInitialLocation = true;
            }
        }
    }

    /**
     * TODO we need to change colors and all dynamically for the friend's location change.
     * @param meetusUserDetail
     */
    public void locationChangedForFriend(MeetusUserDetail meetusUserDetail) {
        Log.i(APPTAG,"Got user details changed for a friend");
        if (meetusUserDetail != null) {
            Log.i(APPTAG,meetusUserDetail.toString());
            Double latitude = Double.parseDouble(meetusUserDetail.getLatitude());
            Double longitude = Double.parseDouble(meetusUserDetail.getLongitude());
            LatLng userLocation = new LatLng(latitude,longitude);
            String userId = meetusUserDetail.getId();
            if (!userMap.containsKey(userId)) {
                MapUser mapUser = new MapUser(userId, new ArrayList<LatLng>());
                if(colors.size() == 0) {
                    mapUser.setUserColor(Color.YELLOW);
                } else {
                    mapUser.setUserColor(colors.get(0));
                    colors.remove(0);
                }
                userMap.put(userId, mapUser);
            }
            MapUser userFromMap = userMap.get(userId);
            userFromMap.getUserLocationList().add(userLocation);
            Marker previousMarker = userFromMap.getLastMarker();
            if (previousMarker != null) {
                previousMarker.remove();
            }
            traceUserPath(userFromMap.getUserLocationList(), userFromMap.getUserColor());
            MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(latitude,
                    longitude)).title(meetusUserDetail.getId());
            Marker marker = mMap.addMarker(markerOptions);
            userFromMap.setLastMarker(marker);
        } else {
            Log.i(APPTAG, "meetusUser detail is null");
        }
    }

    private void traceUserPath(List<LatLng> userLocationList, Integer userColor) {
        if (userLocationList.size() == 1) {
            return;
        }
        LatLng first = userLocationList.get(userLocationList.size()-2);
        LatLng second = userLocationList.get(userLocationList.size()-1);
        Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(first,second )
                .width(8)
                .color(userColor).geodesic(true));
    }

}



